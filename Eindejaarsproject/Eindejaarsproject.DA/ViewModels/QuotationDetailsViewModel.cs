﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
   public  class QuotationDetailsViewModel
    {

        public int Id { get; set; }

        public decimal PriceExclTaxInclCommission { get; set; }

        public decimal PriceInclTaxInclCommission { get; set; }

        public decimal TaxPercentage { get; set; }

        public string ExpirationDate { get; set; }

        public int DeliveryPeriodExpressedInDays { get; set; }

        public String InvoiceCreationDate { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public AddressDetailsViewModel Address { get; set; }
    }
}
