﻿using Eindejaarsproject.DA.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
    public class NoteViewModel
    {
        public int? Id { get; set; }


        [Display(Name = "Note", ResourceType = typeof(Global))]
        public String Content { get; set; }
    }
}
