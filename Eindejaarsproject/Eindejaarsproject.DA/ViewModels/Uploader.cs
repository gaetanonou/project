﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
    public class Uploader
    {
        public Uploader()
        {
            this.Files = new List<File>();
        }
        public List<File> Files { get; set; }

    }

    public class File
    {
        public String Name { get; set; }
        public String BlobName { get; set; }


    }

    
}
