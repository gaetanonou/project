﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
    public class AddressDetailsViewModel
    {
        public int? AddressId { get; set; }

        public String Street { get; set; }

        public String ZipCode { get; set; }


        public String City { get; set; }


        public String State { get; set; }


        public String Country { get; set; }
    }
}
