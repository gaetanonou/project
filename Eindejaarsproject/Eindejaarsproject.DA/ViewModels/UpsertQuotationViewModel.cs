﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Eindejaarsproject.DA.ViewModels
{
    public class UpsertQuotationViewModel
    {

        public int Id { get; set; }

        [Required]
        public String ExpirationDateString { get; set; }

        public DateTime ExpirationDate { get; set; }

        [Required]
        public int DeliveryPeriodExpressedInDays { get; set; }

        [Required]
        public decimal PriceExclTax { get; set; }

        [Required]
        public int ProjectId { get; set; }

        public String AspNetUserId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public AddressDetailsViewModel Address{ get; set; }

        public IEnumerable<QuotationForListViewModel> Quotations { get; set; }
    }
}
