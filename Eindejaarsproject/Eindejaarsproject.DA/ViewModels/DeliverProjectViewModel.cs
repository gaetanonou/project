﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
    public class DeliverProjectViewModel
    {

        public DeliverProjectViewModel()
        {
              this.Uploader = new Uploader();
              this.Note = new NoteViewModel();
        }

        public int Id { get; set; }

        public Uploader Uploader { get; set; }

        public NoteViewModel Note { get; set; }
    }
}
