﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
    public class FreelancerForListViewModel
    {

        public String Id { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String FullName
        {
            get
            {
                return String.Format("{0} {1}", this.FirstName, this.LastName);
            }
        }


        public String Email { get; set; }
    }
}
