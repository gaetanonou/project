﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
    public class DocumentViewModel
    {
        public String Name { get; set; }
        public String BlobName { get; set; }
    }
}
