﻿using Eindejaarsproject.DA.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Eindejaarsproject.DA.Resources;
using System.ComponentModel;

namespace Eindejaarsproject.DA.ViewModels
{
    public class ProjectViewModel
    {

        public int? Id { get; set; }


        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "Required")]
        [Display(Name = "SourceLanguage", ResourceType = typeof(Global))]
        public String SourceLanguageCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "Required")]
        [Display(Name = "TargetLanguage", ResourceType = typeof(Global))]
        public String TargetLanguageCode { get; set; }

        public String CustomerId { get; set; }

        public string FreelancerId { get; set; }

        public Eindejaarsproject.Model.Models.Enums.ProjectStep Step { get; set; }
        public Eindejaarsproject.Model.Models.Enums.PaymentStep PaymentStep { get; set; }

        public int? AcceptedQuotationId { get; set; }
      
        public int InvoiceId { get; set; }

        [Display(Name = "IsSwornTranslation", ResourceType = typeof(Global))]
        public bool IsSwornTranslation { get; set; }

        public int NoteId { get; set; }


        public IEnumerable<SelectListItem> LanguagesForDropdown { get; set; }


        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "Required")]
        [Display(Name = "DesiredDeadline", ResourceType = typeof(Global))]
        public DateTime DesiredDeadline{ get; set; }





        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "Required")]
        [Display(Name = "DesiredDeadline", ResourceType = typeof(Global))]
        public String DesiredDeadlineString { get; set; }
    }
}
