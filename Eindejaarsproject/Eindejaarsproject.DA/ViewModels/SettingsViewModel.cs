﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
    public class SettingsViewModel
    {
        [Required]
        public decimal TaxPercentage { get; set; }

        [Required]
        public decimal CommissionPercentage { get; set; }
    }
}
