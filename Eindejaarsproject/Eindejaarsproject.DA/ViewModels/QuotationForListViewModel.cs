﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
    public class QuotationForListViewModel
    {

        public int Id { get; set; }

        public decimal PriceExlTax { get; set; }

        public decimal PriceExclTaxInclCommission { get; set; }


        public int DeliveryPeriodExpressedInDays { get; set; }

        public String CreationDate { get; set; }

        public String ExpirationDate { get; set; }

        public int ProjectId { get; set; }

    }
}
