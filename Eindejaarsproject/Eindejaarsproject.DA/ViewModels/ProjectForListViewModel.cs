﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eindejaarsproject.DA.Helpers;

namespace Eindejaarsproject.DA.ViewModels
{
    public class ProjectForListViewModel
    {
        public int Id { get; set; }
        public int RowCount { get; set; }

        public String SourceLanguageCode { get; set; }
        public String SourceLanguage { 
          get{

             return Globalization.Languages(this.SourceLanguageCode).FirstOrDefault().Text.ToString();
          }
        }


        public String TargetLanguageCode { get; set; }
        public String TargetLanguage {
            get
            {
                return Globalization.Languages(this.TargetLanguageCode).FirstOrDefault().Text.ToString();
            }
        }
        public String CreationDate { get; set; }


        public Eindejaarsproject.Model.Models.Enums.ProjectStep Step { get; set; }
        public Eindejaarsproject.Model.Models.Enums.PaymentStep PaymentStep { get; set; }


        public String CustomerId { get; set; }

        public bool HasQuotation { get; set; }

        public int? AcceptedQuotationId { get; set; }

        public DateTime? Deadline { get; set; }


        public string FreelancerId { get; set; }
    }
}
