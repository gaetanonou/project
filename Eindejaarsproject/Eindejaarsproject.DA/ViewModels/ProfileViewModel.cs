﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eindejaarsproject.DA.Helpers;

namespace Eindejaarsproject.DA.ViewModels
{
    public class ProfileViewModel
    {

        //public ProfileViewModel()
        //{
        //    this.AddressViewModel = new AddressViewModel();//fout, moet ik ophalen, is niet nieuw
        //}

        public String Email { get; set; }

        public String Name { get; set; }

        public List<String> Languages { get; set; }

        public Uploader Uploader { get; set; }

        public AddressViewModel AddressViewModel { get; set; }

        public IEnumerable<System.Web.Mvc.SelectListItem> LanguagesForDropdown
        {
            get { return Globalization.Languages(null); }

        }
    }


}
