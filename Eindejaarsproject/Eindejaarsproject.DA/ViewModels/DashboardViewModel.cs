﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
    public class DashboardViewModel
    {

        public int FreelancersCount { get; set; }

        public int CustomersCount { get; set; }

        public int ApprovedLanguagesCount { get; set; }

        public int ProjectsCount { get; set; }


    }
}
