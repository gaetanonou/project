﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
    public class UpsertProjectViewModel
    {

        public UpsertProjectViewModel()
        {
            this.Project = new ProjectViewModel();
            this.Note = new NoteViewModel();
            this.Uploader = new Uploader();
        }


        public Uploader Uploader { get; set; }

        public ProjectViewModel Project { get; set; }

        public NoteViewModel Note { get; set; }
    }
}
