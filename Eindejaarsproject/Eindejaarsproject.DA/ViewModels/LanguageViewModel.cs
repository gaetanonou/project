﻿using Eindejaarsproject.DA.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Eindejaarsproject.DA.ViewModels
{
    public class LanguageViewModel
    {
        public String LanguageCode { get; set; }
        
        public String Language { get; set; }

        public bool Approved { get; set; }

        public IEnumerable<SelectListItem> LanguagesForDropdown
        {
            get { return Globalization.Languages(null); }

        }
    }
}
