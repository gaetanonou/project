﻿using Eindejaarsproject.DA.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
    public class ProjectDetailsViewModel
    {
        #region Contstructor
        public ProjectDetailsViewModel()
        {
            this.SourceDocuments = new List<DocumentViewModel>();
            this.TargetDocuments = new List<DocumentViewModel>();
        }
        #endregion


        public String SourceLanguageCode { get; set; }
        public String SourceLanguage
        {
            get
            {
                return Globalization.Languages(this.SourceLanguageCode).FirstOrDefault().Text.ToString();
            }
        }

        public String TargetLanguageCode { get; set; }
        public String TargetLanguage
        {
            get
            {
                return Globalization.Languages(this.TargetLanguageCode).FirstOrDefault().Text.ToString();
            }
        }

        public string CustomerId { get; set; }
        public string FreelancerId { get; set; }

        public Eindejaarsproject.Model.Models.Enums.ProjectStep Step { get; set; }
        public Eindejaarsproject.Model.Models.Enums.PaymentStep PaymentStep { get; set; }
        public bool IsSwornTranslation { get; set; }

        public String Note { get; set; }
        public String CreationDate { get; set; }

        public String DesiredDeadline { get; set; }

        public String Deadline { get; set; }


        [Display(Name ="Quotation")]
        public int? QuotationId { get; set; }

        [Display(Name ="Invcoice")]
        public int? InvoiceId { get; set; }


        public List<DocumentViewModel> SourceDocuments { get; set; }


        public List<DocumentViewModel> TargetDocuments { get; set; }
    }
}
