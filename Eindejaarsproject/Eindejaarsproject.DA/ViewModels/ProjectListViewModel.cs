﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
    public class ProjectListViewModel
    {
        public List<ProjectForListViewModel> ProjectForList { get; set; }
    }
}
