﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Eindejaarsproject.DA.Helpers;

namespace Eindejaarsproject.DA.ViewModels
{
    public class AddressViewModel
    {

        [Required]
        public String Street { get; set; }

        [Required]
        public String ZipCode { get; set; }

        [Required]
        public String City { get; set; }

        [Required]
        public String State { get; set; }

        [Required]
        public String Country { get; set; }

        public IEnumerable<SelectListItem> CountriesForDropdown
        {
            get { return Globalization.Countries(null); }

        }

    }
}
