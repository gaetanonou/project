﻿using Eindejaarsproject.DA.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.ViewModels
{
    public class UserDetailsViewModel
    {
        #region Constructor
        public UserDetailsViewModel()
        {
            this.Address = new AddressDetailsViewModel();
            this.Languages = new List<LanguageViewModel>();
            this.LanguageCodes = new List<string>();
        }

        #endregion

        public String UserId { get; set; }

        public String Email { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String BirthDay { get; set; }

        public AddressDetailsViewModel Address { get; set; }

       
        public List<LanguageViewModel> Languages { get; set; }


        public List<string> LanguageCodes { get; set; }

        public IEnumerable<System.Web.Mvc.SelectListItem> LanguagesForDropdown
        {
            get { return Globalization.Languages(null); }

        }

    }
}
