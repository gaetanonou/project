﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Eindejaarsproject.DA.Helpers
{
    public static class Globalization
    {

        public static IEnumerable<SelectListItem> Countries(string threeLetterISORegionName)
        {

            List<SelectListItem> countryNames = new List<SelectListItem>();
            foreach (CultureInfo cul in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                RegionInfo country = new RegionInfo(new CultureInfo(cul.Name,false).LCID);

                countryNames.Add(new SelectListItem() { Text = country.DisplayName, Value = country.ThreeLetterISORegionName });
            }

            var result = countryNames.GroupBy(m => m.Text).Select(m => m.FirstOrDefault()).ToList<SelectListItem>().OrderBy(m => m.Text).ToList();


            if (!string.IsNullOrEmpty(threeLetterISORegionName))
            {
                result = result.Where(m => m.Value == threeLetterISORegionName).ToList();
            }

            return result;
        }


        public static IEnumerable<SelectListItem> Languages(String threeLetterISOLanguageName)
        {

            List<SelectListItem> languages = new List<SelectListItem>();

            //foreach (CultureInfo cul in CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.NeutralCultures))
            foreach (CultureInfo cul in CultureInfo.GetCultures(CultureTypes.NeutralCultures))
            {
                languages.Add(new SelectListItem() { Text = cul.DisplayName, Value = cul.ThreeLetterISOLanguageName });
            }

            var result = languages.GroupBy(m => m.Value).Select(m => m.FirstOrDefault()).ToList<SelectListItem>().OrderBy(m => m.Text).ToList();


            if(!string.IsNullOrEmpty(threeLetterISOLanguageName))
            {
                result = result.Where(m => m.Value == threeLetterISOLanguageName).ToList();
            }


            return result;
        }

        public static IHtmlString MetaAcceptLanguage()
        {
            var acceptLanguage = HttpUtility.HtmlAttributeEncode(System.Threading.Thread.CurrentThread.CurrentUICulture.ToString());
            return new HtmlString(String.Format(@"<meta name=accept-language content ={0}>",acceptLanguage));
        }

    }
}
