﻿using Eindejaarsproject.DA.IRepositories;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Mime;
using System.Net.Http.Headers;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Eindejaarsproject.DA.Repositories
{
    public class BlobStorageRepository : IBlobStorageRepository
    {
        #region Constructor

        private readonly CloudStorageAccount storageAccount;

        public BlobStorageRepository()
        {
            storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString);
        }
        #endregion

        public string UploadBlobFromLocalFile(string containerName, string blobName, string filePath, string contentType)
        {
            if (String.IsNullOrEmpty(filePath) || !File.Exists(filePath))
                throw new Exception("File is null or stream is empty.");

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer blobContainer = blobClient.GetContainerReference(containerName.ToLower());

            if (blobContainer.CreateIfNotExists())
                blobContainer.SetPermissions(new BlobContainerPermissions() { PublicAccess = BlobContainerPublicAccessType.Container });

            CloudBlockBlob blockFile = blobContainer.GetBlockBlobReference(blobName);
            blockFile.Properties.ContentType = contentType;

            blockFile.UploadFromFile(filePath);

            return blockFile.Name;
        }

        public String UploadBlob(String containerName, String blobName, Stream file)
        {
            if (file == null || file.Length == 0)
                throw new Exception("File is null or stream is empty.");

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer blobContainer = blobClient.GetContainerReference(containerName.ToLower());
            blobContainer.CreateIfNotExists();
            blobContainer.SetPermissions(new BlobContainerPermissions() { PublicAccess = BlobContainerPublicAccessType.Blob });

            CloudBlockBlob blockFile = blobContainer.GetBlockBlobReference(blobName);

            blockFile.UploadFromStream(file);

            return blockFile.Uri.ToString();
        }

        private String CreateNewBlobName(String fileName)
        {
            var originaleFileNameWithoutExt = Path.GetFileNameWithoutExtension(fileName);

            var newFileName = String.Format("{0}_{1}.{2}", originaleFileNameWithoutExt, DateTime.UtcNow.Ticks, Path.GetExtension(fileName));

            return newFileName;
        }

        public CloudBlockBlob CloneBlob(String containerName, String blobName)
        {
            CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer Container = cloudBlobClient.GetContainerReference(containerName.ToLower());

            CloudBlockBlob sourceBlob = Container.GetBlockBlobReference(blobName);
            if (!sourceBlob.Exists())
            {
                return null;
            }
            blobName = CreateNewBlobName(blobName);
            CloudBlockBlob targetBlob = Container.GetBlockBlobReference(blobName);

            targetBlob.StartCopy(sourceBlob);

            return targetBlob;

        }

        public void DeleteBlob(String containerName, String blobName)
        {
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer blobContainer = blobClient.GetContainerReference(containerName.ToLower());
            blobContainer.CreateIfNotExists();
            blobContainer.SetPermissions(new BlobContainerPermissions() { PublicAccess = BlobContainerPublicAccessType.Blob });

            var block = blobContainer.GetBlockBlobReference(blobName);

            if (block.Exists())
            {
                block.Delete();
            }
        }

        
    }
}
