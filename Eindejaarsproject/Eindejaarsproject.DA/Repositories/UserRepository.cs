﻿using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.DA.ViewModels;
using Eindejaarsproject.Model;
using Eindejaarsproject.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eindejaarproject.DA.ViewModels.Identity;
using Eindejaarsproject.Model.Models.Identity;
using Eindejaarsproject.DA.Helpers;

namespace Eindejaarsproject.DA.Repositories
{
    public class UserRepository : IUserRepository
    {
        #region Construct
        private readonly EindejaarsprojectContext dbContext;

        public UserRepository(EindejaarsprojectContext dbContext)
        {
            this.dbContext = dbContext;
        }



        #endregion


        public async Task UpsertUser(String userId, RegisterViewModel model)
        {


            Address address = new Address()
            {
                Street = model.AddressViewModel.Street,
                City = model.AddressViewModel.City,
                ZipCode = model.AddressViewModel.ZipCode,
                Country = model.AddressViewModel.Country,
                State = model.AddressViewModel.State
            };
            dbContext.Address.Add(address);
            await dbContext.SaveChangesAsync();

            UserDetails userDetails = new UserDetails()
            {
                AspNetUserId = userId,
                AddressId = address.Id,
                BirthDay = model.UserDetailsViewModel.BirthDay,
                FristName = model.UserDetailsViewModel.Firstname,
                LastName = model.UserDetailsViewModel.Lastname,
            };

            dbContext.UserDetails.Add(userDetails);
            await dbContext.SaveChangesAsync();

            foreach (var languageCode in model.LanguageCodes)
            {
                FreelancerLanguage freelancerLanguage = new FreelancerLanguage()
                {
                    AspNetUserId = userId,
                    LanguageCode = languageCode,
                    Approved = false

                };
                dbContext.FreelancerLanguages.Add(freelancerLanguage);
            };
            await dbContext.SaveChangesAsync();
        }

        public async Task<UserDetailsViewModel> GetUserDetails(string userId)
        {

            var query = (from u in dbContext.Users

                         join ud in dbContext.UserDetails
                         on u.Id equals ud.AspNetUserId

                         join a in dbContext.Address
                         on ud.AddressId equals a.Id

                         join fl in dbContext.FreelancerLanguages
                         on u.Id equals fl.AspNetUserId
                         into flJoined

                         where u.Id == userId
                         select new
                         {
                             UserId = u.Id,
                             Email = u.Email,
                             FirstName = ud.FristName,
                             LastName = ud.LastName,
                             BirthDay = ud.BirthDay,
                             
                             AddressId = a.Id,
                             Street = a.Street,
                             ZipCode = a.ZipCode,
                             City = a.City,
                             Country = a.Country,
                             State = a.State,

                             Languages = flJoined
                         });


            var result = query.FirstOrDefault();


            if(result != null)
            {
                UserDetailsViewModel model = new UserDetailsViewModel()
                {
                    UserId = result.UserId,
                    Email = result.Email,
                    FirstName = result.FirstName,
                    LastName = result.LastName,
                    BirthDay = result.BirthDay.ToString("dd/MM/yyyy"),
                    Address = new AddressDetailsViewModel()
                    {
                        Street = result.Street,
                        ZipCode = result.ZipCode,
                        City = result.City,
                        Country = Globalization.Countries(result.Country).FirstOrDefault().Text,
                        State = result.State
                    }
                };

                foreach (var language in result.Languages)
                {
                    model.Languages.Add(new LanguageViewModel
                    {
                        LanguageCode = language.LanguageCode,
                        Approved = language.Approved,
                        Language = Globalization.Languages(language.LanguageCode).FirstOrDefault().Text.ToString()

                    });
                }

                return model;
            }
            else
            {
                return null;
            }


        }

        public async Task ApproveLanguages(String userId, List<LanguageViewModel> languages)
        {

            var query = (from fl in dbContext.FreelancerLanguages
                         where fl.AspNetUserId == userId
                         select fl);

            var userLanguages = query.ToList();

            foreach (var language in languages)
            {
                var foundLanguage = userLanguages.Where(m => m.LanguageCode == language.LanguageCode).FirstOrDefault();
                if(foundLanguage != null)
                {
                    if(language.Approved)
                    {
                        foundLanguage.Approved = true;
                    }
                    else
                    {
                        foundLanguage.Approved = false;
                    }
                }
            }
            await dbContext.SaveChangesAsync();
        }

        public Task<UserDetailsViewModel> GetUserDetailsByProjectId(int id)
        {

            var query = (from p in dbContext.Projects
                         where p.Id == id
                         select new {
                             CustomerId = p.CustomerId
                         });

            var result = query.FirstOrDefault();


            return GetUserDetails(result.CustomerId);
        }
    }
}
