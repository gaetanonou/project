﻿using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.DA.ViewModels;
using Eindejaarsproject.Model;
using Eindejaarsproject.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.Repositories
{
    public class SettingsRepository : ISettingsRepository
    {
        #region Constructor

        private readonly EindejaarsprojectContext _dbContext;
        public SettingsRepository(EindejaarsprojectContext dbContext)
        {
            this._dbContext = dbContext;
        }


        #endregion


        public SettingsViewModel GetSettings()
        {
            var query = (from s in _dbContext.Settings
                         select new SettingsViewModel()
                         {
                             CommissionPercentage = s.CommissionPercentage,
                             TaxPercentage = s.TaxPercentage
                         });


            var result = query.FirstOrDefault();


            return result;
        }

        public async Task Update(SettingsViewModel model)
        {
            var query = (from s in _dbContext.Settings
                         select s);



            var result = query.FirstOrDefault();

            if(result == null)
            {
                Setting setting = new Setting()
                {
                    CommissionPercentage = model.CommissionPercentage,
                    TaxPercentage = model.TaxPercentage
                };
                _dbContext.Settings.Add(setting);
            }
            else
            {
                result.CommissionPercentage = model.CommissionPercentage;
                result.TaxPercentage = model.TaxPercentage;
            }




            await _dbContext.SaveChangesAsync();
        }


        public async Task<DashboardViewModel> GetDashboardViewModel(string customerId = null, string freelancerId = null)
        {

            int freelancersCount = (from u in _dbContext.Users

                                    join ud in _dbContext.UserDetails
                                    on u.Id equals ud.AspNetUserId

                                    from ur in u.Roles
                                    join r in _dbContext.Roles
                                    on ur.RoleId equals r.Id
                                    where r.Name == "Freelancer"
                                    select u).Count();

             int customersCount = (from u in _dbContext.Users

                                   join ud in _dbContext.UserDetails
                                   on u.Id equals ud.AspNetUserId

                                   from ur in u.Roles
                                   join r in _dbContext.Roles
                                   on ur.RoleId equals r.Id
                                   where r.Name == "Customer"
                                   select u).Count();

            int projectsCount = 0;
            int approvedLanguagesCount = 0;



            var projectsQuery = (from p in _dbContext.Projects
                                 select new {
                                     CustomerId = p.CustomerId,
                                     FreelancerId = p.FreelancerId
                                 });

            var approvedLanguagesQuery = (from l in _dbContext.FreelancerLanguages
                                          where l.Approved
                                          select new {
                                             LanguageCode = l.LanguageCode,
                                             UserId = l.AspNetUserId
                                          });


            if(string.IsNullOrEmpty(freelancerId) && string.IsNullOrEmpty(customerId))
            {
                projectsCount = projectsQuery.Count();
                approvedLanguagesCount = approvedLanguagesQuery.Distinct().Count();
            }
            if (!string.IsNullOrEmpty(freelancerId))
            {
                projectsCount = projectsQuery.Where(m => m.FreelancerId == freelancerId).Count();
                approvedLanguagesCount = approvedLanguagesQuery.Where(m => m.UserId == freelancerId).Select(m => m.LanguageCode).Distinct().Count();
            }
            if (!string.IsNullOrEmpty(customerId))
            {
                projectsCount = projectsQuery.Where(m => m.CustomerId == customerId).Count();
            }



            DashboardViewModel model = new DashboardViewModel
            {
                ProjectsCount = projectsCount,
                FreelancersCount = freelancersCount,
                CustomersCount = customersCount,
                ApprovedLanguagesCount = approvedLanguagesCount
            };
            return model;

        }
    }
}
