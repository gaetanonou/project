﻿using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eindejaarsproject.DA.ViewModels;
using Eindejaarsproject.Model.Models;
using Eindejaarsproject.DA.Helpers;

namespace Eindejaarsproject.DA.Repositories
{
    public class QuotationRepository : IQuotationRepository
    {

        #region Constructor
        private readonly EindejaarsprojectContext _dbContext;
        private readonly ISettingsRepository _settingsRepository;
 

        public QuotationRepository(EindejaarsprojectContext dbContext, ISettingsRepository settingsRepository)
        {
            this._dbContext = dbContext;
            this._settingsRepository = settingsRepository;
            
        }
        #endregion


        public async Task<UpsertQuotationViewModel> UpsertQuotation(UpsertQuotationViewModel model)
        {
            //Math
            SettingsViewModel settings = _settingsRepository.GetSettings();
            
            //Calc commission
            var commission = (model.PriceExclTax * settings.CommissionPercentage / 100);
            var PriceExclTaxInclCommission = model.PriceExclTax + commission;

            //Calc Tax
            var tax = (model.PriceExclTax * settings.TaxPercentage / 100);
            var PriceInclTaxInclCommission = PriceExclTaxInclCommission + tax;
            Address address = new Address()
            {
                Street = model.Address.Street,
                City = model.Address.City,
                State = model.Address.State,
                ZipCode = model.Address.ZipCode,
                Country = model.Address.Country
            };
            _dbContext.Address.Add(address);
            await _dbContext.SaveChangesAsync();

            Quotation quotation = new Quotation()
            {
                PriceExclTax = model.PriceExclTax,
                PriceExclTaxInclCommission = PriceExclTaxInclCommission,
                PriceInclTaxInclCommission = PriceInclTaxInclCommission,
                TaxPercentage = settings.TaxPercentage,
                ProjectId = model.ProjectId,
                AspNetUserId = model.AspNetUserId,
                CreationDate = DateTime.UtcNow,
                DeliveryPeriodExpressedInDays = model.DeliveryPeriodExpressedInDays,
                ExpirationDate = model.ExpirationDate,

                FirstName = model.FirstName,
                LastName = model.LastName,
                AddressId = address.Id
            };

            _dbContext.Quotations.Add(quotation);
            await _dbContext.SaveChangesAsync();

            return model;
        }


        public async Task<IEnumerable<QuotationForListViewModel>> GetQuotationsForList(string freelancerId = null, int? projectId = null)
        {
            var query = (from q in _dbContext.Quotations

                         join p in _dbContext.Projects
                         on q.ProjectId equals p.Id

                         where p.Step == Enums.ProjectStep.New
                         && q.ExpirationDate >= DateTime.UtcNow

                         select new
                         {
                             Id = q.Id,
                             PriceExlTax = q.PriceExclTax,
                             PriceExclTaxInclCommission = q.PriceExclTaxInclCommission,
                             ProjectId = q.ProjectId,
                             AspNetUserId = q.AspNetUserId,
                             CreationDate = q.CreationDate,
                             DeliveryPeriodExpressedInDays = q.DeliveryPeriodExpressedInDays,
                             ExpirationDate = q.ExpirationDate
                         });

            if (!String.IsNullOrEmpty(freelancerId))
            {
               query = query.Where(m => m.AspNetUserId == freelancerId);

            }

            if (projectId.HasValue)
            {
               query =  query.Where(m => m.ProjectId == projectId);
            }

            var result = query.ToList();

            List<QuotationForListViewModel> models = new List<QuotationForListViewModel>();

            foreach (var item in result)
            {
                QuotationForListViewModel model = new QuotationForListViewModel()
                {
                    Id = item.Id,
                    CreationDate = item.CreationDate.ToString("dd/MM/yyyy"),
                    PriceExlTax = item.PriceExlTax,
                    PriceExclTaxInclCommission = item.PriceExclTaxInclCommission,
                    ProjectId = item.ProjectId,
                    ExpirationDate = item.ExpirationDate.ToString("dd/MM/yyyy"),
                    DeliveryPeriodExpressedInDays = item.DeliveryPeriodExpressedInDays
                };

                models.Add(model);
            };


            return models;
        }

        public async Task<string> AcceptQuotation(int id, string customerId)
        {
            var query = (from q in _dbContext.Quotations

                         join p in _dbContext.Projects
                         on q.ProjectId equals p.Id
                         into joinedP
                         from p in joinedP.DefaultIfEmpty()

                         where q.Id == id
                         && p.CustomerId == customerId
                         && p.Deadline == null
                         select new
                         {
                             q,
                             p
                         });

            var result = query.FirstOrDefault();
            if (result != null)
            {
                Invoice invoice = new Invoice
                {
                    QuotationId = result.q.Id,
                    CreationDate = DateTime.UtcNow,
                };

                result.p.AcceptedQuotationId = result.q.Id;
                result.p.Step = Eindejaarsproject.Model.Models.Enums.ProjectStep.InProgress;
                result.p.PaymentStep = Eindejaarsproject.Model.Models.Enums.PaymentStep.WaitingForPayment;
                result.p.FreelancerId = result.q.AspNetUserId;
                result.p.Deadline = DateTime.UtcNow.AddDays(result.q.DeliveryPeriodExpressedInDays);
                result.p.InvoiceId = invoice.Id;



               

                _dbContext.Invoices.Add(invoice);
                await _dbContext.SaveChangesAsync();

                return result.p.FreelancerId;
            }
            else
            {
                return null;
            }
        }

        public async Task<QuotationDetailsViewModel> GetQuotationDetails(int id)
        {
            var query = (from q in _dbContext.Quotations

                         join i in _dbContext.Invoices
                         on q.Id equals i.QuotationId
                         into iJoined
                         from i in iJoined.DefaultIfEmpty()

                         join a in _dbContext.Address
                         on q.AddressId equals a.Id

                         where q.Id == id

                         select new
                         {
                             Id = q.Id,
                             PriceExclTaxInclCommission = q.PriceExclTaxInclCommission,
                             PriceInclTaxInclCommission = q.PriceInclTaxInclCommission,
                             TaxPercentage = q.TaxPercentage,
                             ExpirationDate = q.ExpirationDate,
                             DeliveryPeriodExpressedInDays = q.DeliveryPeriodExpressedInDays,
                             Invoice = i,
                             Address = a,
                             FirstName = q.FirstName,
                             LastName = q.LastName
                         });

            var result = query.FirstOrDefault();

            if (result == null) return null;


            QuotationDetailsViewModel model = new QuotationDetailsViewModel()
            {
                Id = result.Id,
                FirstName = result.FirstName,
                LastName = result.LastName,
                PriceExclTaxInclCommission = result.PriceExclTaxInclCommission,
                PriceInclTaxInclCommission = result.PriceInclTaxInclCommission,
                DeliveryPeriodExpressedInDays = result.DeliveryPeriodExpressedInDays,
                ExpirationDate = result.ExpirationDate.ToString("dd/MM/yyyy"),
                TaxPercentage = result.TaxPercentage,
                InvoiceCreationDate = result.Invoice ==  null ? null : result.Invoice.CreationDate.ToString("dd/MM/yyyy"),
                Address = new AddressDetailsViewModel
                {
                    City = result.Address.City,
                    Country = result.Address.Country, 
                    State = result.Address.State,
                    Street = result.Address.Street,
                    ZipCode = result.Address.ZipCode
                }
            };

            return model;
        }



    }
}
