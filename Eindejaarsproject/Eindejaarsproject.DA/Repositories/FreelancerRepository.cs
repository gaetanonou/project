﻿using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using Eindejaarsproject.DA.ViewModels;
using Eindejaarsproject.Model.Models;

namespace Eindejaarsproject.DA.Repositories
{
    public class FreelancerRepository : IFreelancerRepository
    {
        #region contructor

        private readonly EindejaarsprojectContext _dbContext;

        public FreelancerRepository(EindejaarsprojectContext dbContext)
        {
            this._dbContext = dbContext;

        }
        #endregion

        public async Task<List<FreelancerForListViewModel>> GetFreelancersForList()
        {
  
            var query = (from u in _dbContext.Users

                        join ud in _dbContext.UserDetails
                        on u.Id equals ud.AspNetUserId

                        from ur in u.Roles
                        join r in _dbContext.Roles 
                        on ur.RoleId equals r.Id
                        where r.Name == "Freelancer"

                        select new  FreelancerForListViewModel
                        {
                          Id = u.Id,
                          FirstName = ud.FristName,
                          LastName =  ud.LastName,
                          Email = u.Email
                        });



      

            var result = query.ToList();
            return result;
        }

        public async Task AddNewLanguages(List<string> Languages, string freelancerId)
        {
            foreach (var item in Languages)
            {
                FreelancerLanguage freelancerLanguage = new FreelancerLanguage
                {
                    AspNetUserId = freelancerId,
                    LanguageCode = item,
                    Approved = false
                };
                _dbContext.FreelancerLanguages.Add(freelancerLanguage);
            }
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<string>> GetLanguagesByFreelancer(string id)
        {
            var query = (from fl in _dbContext.FreelancerLanguages
                         where fl.AspNetUserId == id
                         select fl.LanguageCode
                         );

            var result = query.ToList();
            return result;
        }
    }
}
