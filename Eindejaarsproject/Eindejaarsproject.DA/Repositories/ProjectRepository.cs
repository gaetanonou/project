﻿using Eindejaarsproject.DA.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eindejaarsproject.DA.ViewModels;
using Eindejaarsproject.Model;
using Eindejaarsproject.Model.Models;
using System.Web.Mvc;
using Eindejaarsproject.DA.Helpers;

namespace Eindejaarsproject.DA.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        #region Contstructor

        private readonly EindejaarsprojectContext _dbContext;

        public ProjectRepository(EindejaarsprojectContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public ProjectRepository()
        {
            this._dbContext = new EindejaarsprojectContext();
        }

        #endregion

        public async Task<UpsertProjectViewModel> UpsertProject(UpsertProjectViewModel upsertProjectViewModel)
        {
            if (upsertProjectViewModel.Project.Id.HasValue)
            {
                //Find project for edit;
                Project project = _dbContext.Projects.Find(upsertProjectViewModel.Project.Id);
                project.SourceLanguageCode = upsertProjectViewModel.Project.SourceLanguageCode;
                project.TargetLanguageCode = upsertProjectViewModel.Project.TargetLanguageCode;
                project.IsSwornTranslation = upsertProjectViewModel.Project.IsSwornTranslation;
                project.DesiredDeadline = upsertProjectViewModel.Project.DesiredDeadline;
                project.Step = upsertProjectViewModel.Project.Step;
                project.PaymentStep = upsertProjectViewModel.Project.PaymentStep;

                if (project.NoteId.HasValue)
                {
                    Note note = _dbContext.Notes.Find(upsertProjectViewModel.Note.Id);
                    note.Content = upsertProjectViewModel.Note.Content;
                }

                if (upsertProjectViewModel.Uploader.Files.Any())
                {
                    foreach (var item in upsertProjectViewModel.Uploader.Files)
                    {

                        Blob blob = new Blob()
                        {
                            Name = item.BlobName
                        };

                        _dbContext.Blobs.Add(blob);

                        SourceDocument sourceDocument = new SourceDocument()
                        {
                            Name = item.Name,
                            BlobId = blob.Id,
                            ProjectId = project.Id
                        };

                        _dbContext.SourceDocuments.Add(sourceDocument);
                    }
                }

                await _dbContext.SaveChangesAsync();
            }
            else
            {
                if (!String.IsNullOrEmpty(upsertProjectViewModel.Note.Content))
                {
                    Note note = new Note()
                    {
                        Content = upsertProjectViewModel.Note.Content
                    };

                    _dbContext.Notes.Add(note);
                    await _dbContext.SaveChangesAsync();
                    upsertProjectViewModel.Note.Id = note.Id;
                }

                Project project = new Project()
                {
                    SourceLanguageCode = upsertProjectViewModel.Project.SourceLanguageCode,
                    TargetLanguageCode = upsertProjectViewModel.Project.TargetLanguageCode,
                    IsSwornTranslation = upsertProjectViewModel.Project.IsSwornTranslation,
                    DesiredDeadline = upsertProjectViewModel.Project.DesiredDeadline,
                    CreationDate = DateTime.UtcNow,
                    CustomerId = upsertProjectViewModel.Project.CustomerId,
                    NoteId = upsertProjectViewModel.Note.Id,
                    Step = Enums.ProjectStep.New,
                    PaymentStep = Enums.PaymentStep.WaitingForQuotation
                };

                _dbContext.Projects.Add(project);
                await _dbContext.SaveChangesAsync();


                foreach (var item in upsertProjectViewModel.Uploader.Files)
                {
                    Blob blob = new Blob()
                    {
                        Name = item.BlobName
                    };
                    _dbContext.Blobs.Add(blob);
                    await _dbContext.SaveChangesAsync();

                    SourceDocument sourceDocument = new SourceDocument()
                    {
                        Name = item.Name,
                        BlobId = blob.Id,
                        ProjectId = project.Id
                    };

                    _dbContext.SourceDocuments.Add(sourceDocument);
                    await _dbContext.SaveChangesAsync();
                }
                ;
            }

            return new UpsertProjectViewModel();
        }


        public async Task<ProjectDetailsViewModel> GetProjectDetails(int projectId)
        {
            var query = (from p in _dbContext.Projects
                         join n in _dbContext.Notes
                             on p.NoteId equals n.Id
                             into nJoined

                         from n in nJoined.DefaultIfEmpty()
                         join sd in _dbContext.SourceDocuments
                             on p.Id equals sd.ProjectId
                             into sdJoined

                         join td in _dbContext.TargetDocuments
                             on p.Id equals td.ProjectId
                             into tdJoined

                         join i in _dbContext.Invoices
                         on p.AcceptedQuotationId equals i.QuotationId
                         into joinedI
                         from i in joinedI.DefaultIfEmpty()

                         where p.Id == projectId
                               && !p.IsDeleted
                         select new
                         {
                             p.CustomerId,
                             p.FreelancerId,
                             p.SourceLanguageCode,
                             p.TargetLanguageCode,
                             p.IsSwornTranslation,
                             p.Step,
                             p.PaymentStep,
                             p.CreationDate,
                             p.DesiredDeadline,
                             p.Deadline,
                             p.AcceptedQuotationId,
                             NoteContent = n.Content,
                             InvoiceId = p.InvoiceId,

                             SourceDocuments = (from sd in sdJoined
                                                join b in _dbContext.Blobs
                                                    on sd.BlobId equals b.Id
                                                select new
                                                {
                                                    Name = sd.Name,
                                                    blobName = b.Name
                                                }),
                             TargetDocuments = (from td in tdJoined
                                                join b in _dbContext.Blobs
                                                    on td.BlobId equals b.Id
                                                select new
                                                {
                                                    Name = td.Name,
                                                    blobName = b.Name
                                                })
                         });


            var result = query.FirstOrDefault();

            List<DocumentViewModel> sourceDocuments = new List<DocumentViewModel>();
            foreach (var item in result.SourceDocuments)
            {
                sourceDocuments.Add(new DocumentViewModel
                {
                    Name = item.Name,
                    BlobName = item.blobName
                });
            }

            List<DocumentViewModel> targetDocuments = new List<DocumentViewModel>();
            foreach (var item in result.TargetDocuments)
            {
                targetDocuments.Add(new DocumentViewModel
                {
                    Name = item.Name,
                    BlobName = item.blobName
                });
            }

            ProjectDetailsViewModel model = new ProjectDetailsViewModel()
            {
                CustomerId = result.CustomerId,
                FreelancerId = result.FreelancerId,
                SourceLanguageCode = result.SourceLanguageCode,
                TargetLanguageCode = result.TargetLanguageCode,
                IsSwornTranslation = result.IsSwornTranslation,
                Step = result.Step,
                PaymentStep = result.PaymentStep,
                DesiredDeadline = result.DesiredDeadline.ToString("dd/MM/yyyy"),
                Deadline = result.Deadline.HasValue ? result.Deadline.Value.ToString("dd/MM/yyyy") : null,
                CreationDate = result.CreationDate.ToString("dd/MM/yyyy"),
                Note = result.NoteContent,
                SourceDocuments = sourceDocuments,
                TargetDocuments = targetDocuments,
                QuotationId = result.AcceptedQuotationId,
                InvoiceId = result.InvoiceId
            };


            return model;
        }

        public async Task<IEnumerable<ProjectForListViewModel>> GetProjectsForList(string freelancerId, string customerId)
        {
            var query = (from p in _dbContext.Projects


                         join q in _dbContext.Quotations
                         on p.Id equals q.ProjectId
                         into joinedQ

                         orderby p.CreationDate descending

                         where !p.IsDeleted

                         select new
                         {
                             Id = p.Id,
                             SourceLanguageCode = p.SourceLanguageCode,
                             TargetLanguageCode = p.TargetLanguageCode,
                             Step = p.Step,
                             PaymentStep = p.PaymentStep,
                             CreationDate = p.CreationDate,
                             CustomerId = p.CustomerId,
                             AcceptedQuotationId = p.AcceptedQuotationId,
                             FreelancerId = p.FreelancerId,
                             HasQuotation = joinedQ.Any(),
                             Deadline = p.Deadline
                         });


            //Filter
            if (!String.IsNullOrEmpty(freelancerId))
            {

                query = (from q in query

                         join fl in _dbContext.FreelancerLanguages
                         on freelancerId equals fl.AspNetUserId
                         into joinedFl


                         where
                         joinedFl.Any(m => m.LanguageCode == q.SourceLanguageCode && m.Approved)
                         && joinedFl.Any(m => m.LanguageCode == q.TargetLanguageCode && m.Approved)
                         && (q.FreelancerId == freelancerId || q.FreelancerId == null)
                         

                         select q);
            }
            if (!String.IsNullOrEmpty(customerId))
            {
                query = query.Where(m => m.CustomerId == customerId);
            }


            var result = query.ToList();

            List<ProjectForListViewModel> models = new List<ProjectForListViewModel>();
            foreach (var item in result)
            {
                ProjectForListViewModel model = new ProjectForListViewModel()
                {
                    Id = item.Id,
                    Step = item.Step,
                    PaymentStep = item.PaymentStep,
                    CreationDate = item.CreationDate.ToString("dd/MM/yyyy"),
                    CustomerId = item.CustomerId,
                    SourceLanguageCode = item.SourceLanguageCode,
                    TargetLanguageCode = item.TargetLanguageCode,
                    AcceptedQuotationId = item.AcceptedQuotationId,
                    HasQuotation = item.HasQuotation,
                    Deadline = item.Deadline,
                    FreelancerId = item.FreelancerId
                };

                models.Add(model);
            }


            return models;
        }

        public async Task<bool> DeleteProject(int id)
        {
            var query = (from p in _dbContext.Projects
                         where p.Id == id
                         select p);

            var project = query.FirstOrDefault();

            if (project == null)
            {
                return false;
            }
            else
            {
                project.IsDeleted = true;
                await _dbContext.SaveChangesAsync();

                return true;
            }
        }

        public async Task<UpsertProjectViewModel> GetUpsertProjectViewModel(int projectId)
        {
            var query = (from p in _dbContext.Projects
                         join n in _dbContext.Notes
                             on p.NoteId equals n.Id
                             into nJoined
                         from n in nJoined.DefaultIfEmpty()
                         where p.Id == projectId
                               && !p.IsDeleted
                         select new UpsertProjectViewModel()
                         {
                             Project = new ProjectViewModel()
                             {
                                 Id = p.Id,
                                 SourceLanguageCode = p.SourceLanguageCode,
                                 TargetLanguageCode = p.TargetLanguageCode,
                                 IsSwornTranslation = p.IsSwornTranslation,
                                 DesiredDeadline = p.DesiredDeadline,
                                 Step = p.Step,
                                 PaymentStep = p.PaymentStep
                             },
                             Note = new NoteViewModel()
                             {
                                 Id = n.Id,
                                 Content = n.Content
                             }
                         });

            var result = query.FirstOrDefault();

            if (result != null)
            {
                result.Project.DesiredDeadlineString = result.Project.DesiredDeadline.ToString("dd/MM/yyyy");
            }

            return result;
        }

        public async Task<IEnumerable<SelectListItem>> GetApprovedLanguagesForDropDown()
        {
            var query = (from fl in _dbContext.FreelancerLanguages
                         where fl.Approved
                         select new
                         {
                             LanguageCode = fl.LanguageCode
                         }).GroupBy(m => m.LanguageCode);

            var result = query.ToList();

            List<SelectListItem> languages = new List<SelectListItem>();

            foreach (var item in result)
            {
                var code = item.Key.ToString();
                languages.Add(new SelectListItem()
                {
                    Text = Globalization.Languages(code).FirstOrDefault().Text.ToString(),
                    Value = code
                });
            }

            return languages.OrderBy(m => m.Text);
        }

        public async Task Deliver(DeliverProjectViewModel deliverProjectViewModel)
        {
            var project = (from p in _dbContext.Projects
                           where p.Id == deliverProjectViewModel.Id
                           select p).FirstOrDefault();

            project.Step = Enums.ProjectStep.Delivered;


            foreach (var item in deliverProjectViewModel.Uploader.Files)
            {
                Blob blob = new Blob()
                {
                    Name = item.BlobName
                };
                _dbContext.Blobs.Add(blob);
                await _dbContext.SaveChangesAsync();

                TargetDocument targetDocument = new TargetDocument()
                {
                    Name = item.Name,
                    BlobId = blob.Id,
                    ProjectId = deliverProjectViewModel.Id
                };

                _dbContext.TargetDocuments.Add(targetDocument);
                await _dbContext.SaveChangesAsync();
            }
            ;
        }

        public async Task<DateTime> GetDeadlineForProject(int projectId)
        {
            var query = (from p in _dbContext.Projects
                where p.Id == projectId
                select p.Deadline);

            var result = query.FirstOrDefault();

            if (result == null)
            {
                return DateTime.UtcNow;
            }
            else
            {
                return result.Value;
            }
        }

        public async Task<IEnumerable<SelectListItem>> GetMatchingLanguagesForAPI(string languageCode)
        {
            var query = (from u in _dbContext.Users

                         join fl in _dbContext.FreelancerLanguages
                         on u.Id equals fl.AspNetUserId

                         join flu in _dbContext.FreelancerLanguages
                         on fl.AspNetUserId equals flu.AspNetUserId

                         where fl.LanguageCode == languageCode 
                         && flu.LanguageCode != languageCode
                         && flu.Approved

                         select flu.LanguageCode).Distinct();



            var result = query.ToList();

            List<SelectListItem> languages = new List<SelectListItem>();
            foreach (var item in result)
            {
                languages.Add(new SelectListItem
                {
                    Value = item,
                    Text = Globalization.Languages(item).FirstOrDefault().Text.ToString()
                });
            }

            return languages.OrderBy(m => m.Text);
        }
    }
}