﻿using Eindejaarsproject.DA.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.IRepositories
{
    public interface ISettingsRepository
    {
        SettingsViewModel GetSettings();
        Task Update(SettingsViewModel model);

        Task<DashboardViewModel> GetDashboardViewModel(string customerId = null, string freelancerId = null);
    }
}
