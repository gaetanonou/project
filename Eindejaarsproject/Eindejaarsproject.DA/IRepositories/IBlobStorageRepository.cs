﻿
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.IRepositories
{
    public interface IBlobStorageRepository
    {
        string UploadBlobFromLocalFile(string containerName, string blobName, string filePath, string contentType);
        string UploadBlob(String containerName, String blobName, Stream file);
        CloudBlockBlob CloneBlob(String containerName, String blobName);
        void DeleteBlob(String containerName, String blobName);
        
    }
}
