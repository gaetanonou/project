﻿using Eindejaarsproject.DA.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Eindejaarsproject.DA.IRepositories
{
    public interface IProjectRepository
    {
        Task<UpsertProjectViewModel> UpsertProject(UpsertProjectViewModel upsertProjectViewModel);
        Task<ProjectDetailsViewModel> GetProjectDetails(int projectId);
        Task<UpsertProjectViewModel> GetUpsertProjectViewModel(int projectId);
        Task<IEnumerable<ProjectForListViewModel>> GetProjectsForList(String freelancerId, String customerId);
        Task<bool> DeleteProject(int id);
        Task<IEnumerable<SelectListItem>> GetApprovedLanguagesForDropDown();
        Task Deliver(DeliverProjectViewModel deliverProjectViewModel);
        Task<DateTime> GetDeadlineForProject(int projectId);
        Task<IEnumerable<SelectListItem>> GetMatchingLanguagesForAPI(string languageCode);
    }
}
