﻿using Eindejaarproject.DA.ViewModels.Identity;
using Eindejaarsproject.DA.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.IRepositories
{
    public interface IUserRepository
    {
        Task UpsertUser(String userId, RegisterViewModel model);

        Task<UserDetailsViewModel> GetUserDetails(String userId);

        Task ApproveLanguages(String userId,  List<LanguageViewModel> languages);

        Task<UserDetailsViewModel> GetUserDetailsByProjectId(int id);

    }
}
