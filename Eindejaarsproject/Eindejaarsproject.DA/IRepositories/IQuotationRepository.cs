﻿using Eindejaarsproject.DA.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.IRepositories
{
    public interface IQuotationRepository
    {
        Task<UpsertQuotationViewModel> UpsertQuotation(UpsertQuotationViewModel model);
        Task<IEnumerable<QuotationForListViewModel>> GetQuotationsForList(String freelancerId = null,int? projectId = null);
        Task<string> AcceptQuotation(int id, string customerId);
        Task<QuotationDetailsViewModel> GetQuotationDetails(int id);
    }
}
