﻿using Eindejaarsproject.DA.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.DA.IRepositories
{
    public interface IFreelancerRepository
    {

        Task<List<FreelancerForListViewModel>> GetFreelancersForList();
        Task AddNewLanguages(List<string> Languages, string freelancerId);
        Task<List<string>> GetLanguagesByFreelancer(string id);

    }
}
