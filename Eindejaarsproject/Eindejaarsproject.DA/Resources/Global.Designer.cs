﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Eindejaarsproject.DA.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Global {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Global() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Eindejaarsproject.DA.Resources.Global", typeof(Global).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Interpreter Talk is an online translation platform. This is a web application for managing translation projects. The translation platform provides our customers with convenience, speed , safety and benefit..
        /// </summary>
        public static string AboutUs {
            get {
                return ResourceManager.GetString("AboutUs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to About Us.
        /// </summary>
        public static string AboutUs_Title {
            get {
                return ResourceManager.GetString("AboutUs_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accept.
        /// </summary>
        public static string Accept {
            get {
                return ResourceManager.GetString("Accept", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accept quotation.
        /// </summary>
        public static string AcceptQuotation {
            get {
                return ResourceManager.GetString("AcceptQuotation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to accept this offer of &amp;euro; &lt;span class=&quot;bold&quot; id=&quot;price&quot;&gt;&lt;/span&gt; with a closing deadline &lt;span class=&quot;bold&quot; id=&quot;deadline&quot;&gt;&lt;/span&gt; ?.
        /// </summary>
        public static string AcceptQuotationModalBody {
            get {
                return ResourceManager.GetString("AcceptQuotationModalBody", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact.
        /// </summary>
        public static string Contact {
            get {
                return ResourceManager.GetString("Contact", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create.
        /// </summary>
        public static string Create {
            get {
                return ResourceManager.GetString("Create", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Creation date.
        /// </summary>
        public static string CreationDate {
            get {
                return ResourceManager.GetString("CreationDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dashboard.
        /// </summary>
        public static string Dashboard {
            get {
                return ResourceManager.GetString("Dashboard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Desired deadline.
        /// </summary>
        public static string DesiredDeadline {
            get {
                return ResourceManager.GetString("DesiredDeadline", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Desk.
        /// </summary>
        public static string Desk {
            get {
                return ResourceManager.GetString("Desk", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Details.
        /// </summary>
        public static string Details {
            get {
                return ResourceManager.GetString("Details", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Executable Deadline.
        /// </summary>
        public static string ExecutableDeadline {
            get {
                return ResourceManager.GetString("ExecutableDeadline", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Home.
        /// </summary>
        public static string Home {
            get {
                return ResourceManager.GetString("Home", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sworn translation.
        /// </summary>
        public static string IsSwornTranslation {
            get {
                return ResourceManager.GetString("IsSwornTranslation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Languages.
        /// </summary>
        public static string Languages {
            get {
                return ResourceManager.GetString("Languages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to List.
        /// </summary>
        public static string List {
            get {
                return ResourceManager.GetString("List", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log in.
        /// </summary>
        public static string LogIn {
            get {
                return ResourceManager.GetString("LogIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log off.
        /// </summary>
        public static string LogOff {
            get {
                return ResourceManager.GetString("LogOff", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New project.
        /// </summary>
        public static string NewProject {
            get {
                return ResourceManager.GetString("NewProject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to News.
        /// </summary>
        public static string News {
            get {
                return ResourceManager.GetString("News", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Note.
        /// </summary>
        public static string Note {
            get {
                return ResourceManager.GetString("Note", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Price excl. Tax.
        /// </summary>
        public static string PriceExclTax {
            get {
                return ResourceManager.GetString("PriceExclTax", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profile.
        /// </summary>
        public static string Profile {
            get {
                return ResourceManager.GetString("Profile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Projects.
        /// </summary>
        public static string Projects {
            get {
                return ResourceManager.GetString("Projects", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quotation.
        /// </summary>
        public static string Quotation {
            get {
                return ResourceManager.GetString("Quotation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quotations.
        /// </summary>
        public static string Quotations {
            get {
                return ResourceManager.GetString("Quotations", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register.
        /// </summary>
        public static string Register {
            get {
                return ResourceManager.GetString("Register", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Source language.
        /// </summary>
        public static string SourceLanguage {
            get {
                return ResourceManager.GetString("SourceLanguage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Step.
        /// </summary>
        public static string Step {
            get {
                return ResourceManager.GetString("Step", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Target language.
        /// </summary>
        public static string TargetLanguage {
            get {
                return ResourceManager.GetString("TargetLanguage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome to Tolk Talk.
        /// </summary>
        public static string Welcome_Title {
            get {
                return ResourceManager.GetString("Welcome_Title", resourceCulture);
            }
        }
    }
}
