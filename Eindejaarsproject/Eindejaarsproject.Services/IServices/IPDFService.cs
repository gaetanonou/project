﻿using Eindejaarsproject.DA.ViewModels;
using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.Services.IServices
{
    public interface IPDFService
    {
        MemoryStream CreateQuotation(QuotationDetailsViewModel quotationDetails, bool isInvoice);

    }
}
