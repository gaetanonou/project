﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.Services.IServices
{
    public interface IMailService
    {

        Task SendConfirmationMail(string userFullName, string email, string emailConfirmationLink);
        Task SendMail(string userFullName,string[] recipients, string subject, string text);
    }
}
