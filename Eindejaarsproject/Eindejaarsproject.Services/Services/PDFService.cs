﻿
using Eindejaarsproject.DA.ViewModels;
using Eindejaarsproject.Model;
using Eindejaarsproject.Services.IServices;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.IO;
using System.Web;

namespace Eindejaarsproject.Services.Services
{
    public class PDFService : IPDFService
    {

        #region Constructor
        private readonly EindejaarsprojectContext _dbContext;

        public PDFService(EindejaarsprojectContext dbContext)
        {
            this._dbContext = dbContext;
        }



        #endregion




        public MemoryStream CreateQuotation(QuotationDetailsViewModel quotationDetails, bool isInvoice)
        {

            var document = new Document(PageSize.A4, 50, 50, 25, 25);

            // Create a new PdfWriter object, specifying the output stream
            var output = new MemoryStream();
            var writer = PdfWriter.GetInstance(document, output);

            // Open the Document for writing
            document.Open();



            string contents = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(
                (isInvoice ? "~/Templates/PDF/InvoiceTemplate.html" : "~/Templates/PDF/QuotationTemplate.html") ));

            // Replace the placeholders with the user-specified text

            if(isInvoice)
            {
                contents = contents.Replace("[CREATIONDATE]", quotationDetails.InvoiceCreationDate);
                
            }
            else
            {
                contents = contents.Replace("[EXPIRATIONDATE]", quotationDetails.ExpirationDate);
                contents = contents.Replace("[DELIVERYPERDIODEXPRESSEDINDAYS]", quotationDetails.DeliveryPeriodExpressedInDays.ToString());
            }
         

            contents = contents.Replace("[FIRSTNAME]", quotationDetails.FirstName);
            contents = contents.Replace("[LASTNAME]", quotationDetails.LastName);

            contents = contents.Replace("[STREET]", quotationDetails.Address.Street);
            contents = contents.Replace("[CITY]", quotationDetails.Address.City);
            contents = contents.Replace("[ZIPCODE]", quotationDetails.Address.ZipCode);
            contents = contents.Replace("[STATE]", quotationDetails.Address.State);
            contents = contents.Replace("[COUNTRY]", quotationDetails.Address.Country);




            contents = contents.Replace("[PRICEEXCL]", quotationDetails.PriceExclTaxInclCommission.ToString());
            contents = contents.Replace("[TAX]", quotationDetails.TaxPercentage.ToString());
            contents = contents.Replace("[PRICEINCLTAX]", quotationDetails.PriceInclTaxInclCommission.ToString());



            // Step 4: Parse the HTML string into a collection of elements...
            var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(contents), null);

            // Enumerate the elements, adding each one to the Document...
            foreach (var htmlElement in parsedHtmlElements)
                document.Add(htmlElement as IElement);

            var logo = Image.GetInstance(HttpContext.Current.Server.MapPath("~/Content/images/logo.png"));
            logo.ScaleToFit(150, 150);


            logo.SetAbsolutePosition(20, 720);
            document.Add(logo);


            document.Close();

            return output;
        }



    }
}
