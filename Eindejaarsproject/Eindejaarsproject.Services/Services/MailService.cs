﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Mail;
using Eindejaarsproject.Services.IServices;
using SendGrid;
using Microsoft.AspNet.Identity;
using Exceptions;
using System.Text;

namespace Eindejaarsproject.Services.Services
{

    public class MailService : IMailService
    {
        private readonly Web transportWeb;
        public MailService()
        {
            this.transportWeb = new Web("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");
        }

        public async Task SendConfirmationMail(string userFullName, string emailaddress, string emailConfirmationLink)
        {
            try
            {
                var mail = new SendGridMessage();
                mail.AddTo(emailaddress);
                mail.From = new MailAddress("info@tolktalk.onou.net", "TolkTalk");
                mail.Subject = "TolkTalk confirmation email";
                mail.Text = "Thank you for signing up!";
                mail.Html = "<span>Thank you for signing up! Click on the link below to confirm your emailaddress.</span>";
                mail.AddSubstitution("-greeting-", new List<string>() { "Hi" });
                mail.AddSubstitution("-userfullname-", new List<string>() { userFullName });
                mail.AddSubstitution("-confirmationlinktext-", new List<string>() { "confirm" });
                mail.AddSubstitution("-confirmationlink-", new List<string>() { emailConfirmationLink });

                var filters = new Dictionary<string, dynamic>()
                {
                    {
                        "templates", new Dictionary<string, dynamic>()
                        {
                            {
                                "settings", new Dictionary<string, dynamic>()
                                {
                                    {
                                        "enable", "1"
                                    },
                                    {
                                        "template_id", "a9cb5979-42eb-4514-acf3-4d08c88b82b4"
                                    }
                                }
                            }
                        }
                    }
                };
                foreach (var filter in filters.Keys)
                {
                    var settings = filters[filter]["settings"];
                    foreach (var setting in settings.Keys)
                    {
                        mail.Header.AddFilterSetting(filter, new List<string> { setting }, Convert.ToString(settings[setting]));
                    }
                }
                await transportWeb.DeliverAsync(mail);
            }
            catch (InvalidApiRequestException ex)
            {
                var detalle = new StringBuilder();

                detalle.Append("ResponseStatusCode: " + ex.ResponseStatusCode + ".   ");
                for (int i = 0; i < ex.Errors.Count(); i++)
                {
                    detalle.Append(" -- Error #" + i.ToString() + " : " + ex.Errors[i]);
                }

                throw new ApplicationException(detalle.ToString(), ex);
            }
        }

        public async Task SendMail(string userFullName, string[] recipients, string subject, string text)
        {
            try
            {
                var mail = new SendGridMessage();
                mail.AddTo(recipients);
                mail.From = new MailAddress("info@tolktalk.onou.net", "TolkTalk");
                mail.Subject = subject;
                mail.Text = "";
                mail.Html = string.Format("<span>{0}</span>", text);
                mail.AddSubstitution("-greeting-", new List<string>() { "Hi" });
                mail.AddSubstitution("-userfullname-", new List<string>() { userFullName });
             
                var filters = new Dictionary<string, dynamic>()
                {
                    {
                        "templates", new Dictionary<string, dynamic>()
                        {
                            {
                                "settings", new Dictionary<string, dynamic>()
                                {
                                    {
                                        "enable", "1"
                                    },
                                    {
                                        "template_id", "f78f2fc1-4407-4b1f-b62c-8c8c2decf12c"
                                    }
                                }
                            }
                        }
                    }
                };
                foreach (var filter in filters.Keys)
                {
                    var settings = filters[filter]["settings"];
                    foreach (var setting in settings.Keys)
                    {
                        mail.Header.AddFilterSetting(filter, new List<string> { setting }, Convert.ToString(settings[setting]));
                    }
                }
                await transportWeb.DeliverAsync(mail);
            }
            catch (InvalidApiRequestException ex)
            {
                var detalle = new StringBuilder();

                detalle.Append("ResponseStatusCode: " + ex.ResponseStatusCode + ".   ");
                for (int i = 0; i < ex.Errors.Count(); i++)
                {
                    detalle.Append(" -- Error #" + i.ToString() + " : " + ex.Errors[i]);
                }

                throw new ApplicationException(detalle.ToString(), ex);
            }
        }
    }
}
