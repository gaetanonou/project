﻿using System.Web;
using System.Web.Optimization;

namespace Eindejaarproject.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                    "~/Scripts/App/angular-1.5.3/angular.min.js",
                    "~/Scripts/App/angular-1.5.3/angular-resource.min.js",
                    "~/Scripts/App/app.js",
                  "~/Scripts/App/Controllers/projectController.js",
                    "~/Scripts/App/Controllers/uploadController.js",
                    "~/Scripts/App/Services/projectService.js",
                    "~/Scripts/App/Services/uploadService.js"
                ));


            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
                "~/Scripts/plupload.full.min.js",
                "~/Scripts/bootstrap-datepicker-1.5.1/js/bootstrap-datepicker.min.js",
                "~/Scripts/select2.min.js",
                "~/Scripts/sweetalert.min.js",
                "~/Scripts/App/site.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-datepicker-1.5.1/bootstrap-datepicker3.min.css",
                      "~/Content/simple-sidebar.css",
                      "~/Content/select2.min.css",
                      "~/Content/sweetalert.css",
                      "~/Content/font-awesome-4.5.0/css/font-awesome.min.css",
                      "~/Content/base-layout.css",
                      "~/Content/Site.css"));
        }
    }
}
