﻿


$(document).ready(function () {
    //Get Globalization
    var data = $("meta[name='accept-language']").attr("content");

    /*
     *   Select2 Plugin
     */
    $('.select2').select2({
        width:'100%'
    });



    /*
     * Toggle Sidebar
     */
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#menu-toggle").children('i').toggleClass('fa-toggle-on fa-toggle-off ');
    });


    $('body').on('click', '.delete', function (e) {
        var url = $(this).attr('href');

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            closeOnCancel : true
        }, function (isConfirm) {

            if (isConfirm) {
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                window.location = url;
                return true;
            } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
            }
        });
        e.preventDefault();
    });


    /*Bootstrap datepicker*/
    $('.input-group.date').datepicker({
        forceParse: false,
        todayHighlight: true,
        toggleActive: true,
        format: 'dd/mm/yyyy',
    });

    var count;
    //Function to copy language dropdown
    $('#addLanguage').click(function () {
        $('select.select2').select2('destroy');

        var clonedItem = $('#block-to-clone').clone();
        if (count == null) {
            count = parseInt(clonedItem.attr('data-count')) + 1;
        }
        else {
           count = count +1;
        }

        clonedItem.attr('data-count', count);
        clonedItem.find('select').attr('id', 'LanguageCodes_' + count + '_');
        clonedItem.find('select').attr('name', 'LanguageCodes[' + count + ']');
        
        clonedItem.appendTo('#appendLanguage');
        $('select.select2').select2();
    });
});




// Function getSelectedQuoteValue for modal

function setValuesForAcceptQuotationModal(PriceExlTax, ExecutableDeadline, id) {
    $('#price').html(parseFloat(PriceExlTax).toFixed(2));
    $('#deadline').html(ExecutableDeadline);
    $('#acceptQuotation').attr('href', "/Customer/Quotation/AcceptQuotation/"+ id)

}




