﻿var app = angular.module('app.Services');

app.service('projectService', ['$resource', function ($resource) {
 
    this.getMatchingLanguages = function (selectedLanguage) {
        var data = $resource('/api/project/getMatchingLanguages', {}, {
            get: { method: 'get', isArray:true, params: { languageCode: selectedLanguage } }
        });
        return data;
    };

}]); 