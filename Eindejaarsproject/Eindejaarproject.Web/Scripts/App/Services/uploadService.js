﻿var app = angular.module('app.Services');

app.service('uploadService', ['$resource', function ($resource) {
    this.delete = function (blobName) {
        var data = $resource('/api/File/DeleteBlob', {}, {
            post: { method: 'Post', isArray:true, params: {blobName: blobName }}
        });
        return data;
    };
}]);