﻿var controllers = angular.module('app.Controllers', []);
var services = angular.module('app.Services', []);

var app = angular.module('app', [
    'ngResource',
    'app.Controllers',
    'app.Services'
]);