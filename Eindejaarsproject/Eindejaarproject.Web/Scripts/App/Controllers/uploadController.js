﻿var app = angular.module('app.Controllers');

app.controller('uploadController', ['$scope', 'uploadService', function ($scope, uploadService) {
    

    /*
     * Scopes
     */
     
    $scope.files = [];
    $scope.error = false; //shows error on upload.



    /*
     *Funcs 
     */

    $scope.init = function (files) {
        $scope.files = files;
    };

    function deleteFromFiles(obj) {
        var i = $scope.files.indexOf(obj);
        if (i != -1) {
            $scope.files.splice(i, 1);
        };
    }

    $scope.deleteBlob = function (file) {
        file.loading = true;
        uploadService.delete(file.blobName).post(
         function () {
             deleteFromFiles(file);
         },
         function () { console.log("failed") });
       file.loading = true;
    }
     
    /*
     * Uploader
     */

    ////$scope.initUploader = function (customerName, limit, galleryId) {
    $scope.initUploader = function () {
        var limit = 50;
        var uploader = new plupload.Uploader({
            runtimes: 'html5,flash,silverlight,html4',
            browse_button: 'pickfiles', // you can pass in id...
            container: document.getElementById('file_container'), // ... or DOM Element itself
            url: '/Api/File/Upload',
            flash_swf_url: '~/Scripts/Plugins/Moxie.swf',
            //chunk_size:'200kb',
            silverlight_xap_url: '~/Scripts/Moxie.xap',
            file_data_name: "file",
            filters: {
                max_file_size: '100mb',
                mime_types: [
                    { title: "files", extensions: "jpg,jpeg,pjpeg,png,bmp,pdf,doc,docx,txt" }
                ]
            },

            init: {
                PostInit: function () {
                    document.getElementById('filelist').innerHTML = '';
                },

                FilesAdded: function (up, files) {
                    if (files.length > limit) {
                        uploader.splice(limit, files.length - limit);
                        files.splice(limit, files.length - limit);
                        $scope.$apply(function () {
                            $scope.limitReached = true;
                        });
                    }

                    plupload.each(files, function (file) {
                        //document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                        document.getElementById('filelist').innerHTML += '<div id="' + file.id + file.name + '">' + file.name + ' (' + plupload.formatSize(file.size) + ')'
                            + '</div><div id="' + file.id + '" class="progress pos-rel" data-percent="0%"></div>';
                    });

                    $scope.$apply(function () {
                        $scope.busyUploading = true;
                    });

                    uploader.start();
                },

                UploadProgress: function (up, file) {
                    //document.getElementById(file.id).innerHTML = '<span>' + file.percent + "%</span>";
                    var element = document.getElementById(file.id);
                    element.innerHTML = '<div class="progress-bar" style="width:' + file.percent + '%;"></div>';

                    if (file.percent == 100) {
                        element.setAttribute("data-percent", "Afbeelding is aan het verwerken.");
                    } else {
                        element.setAttribute("data-percent", file.percent + '%');
                    }
                },

                Error: function (up, err) {
               
                    //document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;

                },
                FileUploaded: function (uploader, file, object) {
                    var myData;
                    var obj;
                    try {
                        myData = eval(object.response);


                    } catch (err) {
                    }

                    var element = document.getElementById(file.id);
                    element.setAttribute("data-percent", 'OK');

                    if (object.response != null) {
                        obj = JSON.parse(object.response);
                        var fileForList = {
                            name: file.name,
                            blobName: obj
                        };

                        $scope.files.push(fileForList);
                        console.log($scope.files);
                    }


                    var element = document.getElementById(file.id + file.name);
                    element.parentNode.removeChild(element);

                    element = document.getElementById(file.id);
                    element.parentNode.removeChild(element);

                },
                UploadComplete: function (herp, files) {
                    $scope.$apply(function () {
                        $scope.savingFiles = true;
                        $scope.busyUploading = false;
                    });
                    uploader.splice();


                }
            }
        });

        uploader.init();



        ///*Initializting the drop-zones*/
        var dropzone = new mOxie.FileDrop({
           drop_zone: document.getElementById("#file-dropzone")
        });

        dropzone.ondrop = function (event) {
            uploader.addFile(dropzone.files);

        //   $("#attachementsModal").modal("show");
        }
        dropzone.init();
    }

}]);