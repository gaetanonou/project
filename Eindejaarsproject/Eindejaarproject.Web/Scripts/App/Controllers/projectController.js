﻿var app = angular.module('app.Controllers');

app.controller('projectController', ['$scope', 'projectService', function ($scope, projectService) {
    /*
     * Scopes
     */
    $scope.loading = false; 
    $scope.selectedLanguage = "";
    $scope.matchedLanguages = [];
    //$scope.selectedTargetLanguage = "";

    /*
     * Watches
     */

    $scope.$watch('selectedLanguage', function () {
        if ($scope.selectedLanguage) {
            //$scope.selectedTargetLanguage = "";
            getMatchingLanguages($scope.selectedLanguage);
        };
    });



    /*
     * Funcs 
     */


function getMatchingLanguages(selectedLanguage) {
        $scope.loading = true;
        projectService.getMatchingLanguages(selectedLanguage).get(
            function (data) {
                $scope.matchedLanguages = data;
               
            },function () {
                console.log('failed');
                });
        $scope.loading = false;
};







}]);