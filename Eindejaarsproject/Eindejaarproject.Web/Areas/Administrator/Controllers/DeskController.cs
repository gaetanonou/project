﻿using Eindejaarproject.Web.Controllers;
using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.Services.IServices;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Eindejaarproject.Web.Areas.Administrator.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class DeskController : BaseController
    {
        private readonly ISettingsRepository _settingsRepository;
 

        public DeskController(ISettingsRepository settingsRepository)
        {
            this._settingsRepository = settingsRepository;
        }
        // GET: Administrator/Desk
        public async Task<ActionResult> Dashboard()
        {
            var model = await _settingsRepository.GetDashboardViewModel();


            return View(model);
        }
    }
}