﻿using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.DA.ViewModels;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Eindejaarproject.Web.Controllers;

namespace Eindejaarproject.Web.Areas.Administrator.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class FreelancerController : BaseController
    {


        #region Contstructor
        private readonly IFreelancerRepository freelancerRepository;
        private readonly IUserRepository userRepository;

        public FreelancerController(IFreelancerRepository freelancerRepository, IUserRepository userRepository)
        {
            this.freelancerRepository = freelancerRepository;
            this.userRepository = userRepository;
        }
        #endregion

        public async Task<ActionResult> List()
        {
            var model = await freelancerRepository.GetFreelancersForList();

            return View(model);
        }


        public async Task<ActionResult> Details(String id)
        {
            var model = await userRepository.GetUserDetails(id);

            return View(model);
        }


        public async Task<ActionResult> ApproveLanguages(UserDetailsViewModel model)
        {

            await userRepository.ApproveLanguages(model.UserId, model.Languages);
            AddDataToTempData("Succes", "Languages have been updated.");
            return RedirectToAction("List");
        }
    }
}