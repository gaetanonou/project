﻿using Eindejaarproject.Web.Controllers;
using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.DA.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Eindejaarproject.Web.Areas.Administrator.Controllers
{
    [Authorize(Roles ="Administrator")]
    public class SettingsController : BaseController
    {

        #region Constructor

        private readonly ISettingsRepository _settingsRepository;

        public SettingsController(ISettingsRepository settingsRepository)
        {
            this._settingsRepository = settingsRepository;
        }
        #endregion
        [HttpGet]
        public async Task<ActionResult> Update()
        {
            var model = _settingsRepository.GetSettings();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(SettingsViewModel model)
        {

            if (ModelState.IsValid)
            {
                await _settingsRepository.Update(model);
                AddDataToTempData("Succes", "Settings have been updated");
                return RedirectToAction("Update");
        
            }
            else
            {

                AddDataToTempData("Danger", "Error while updating settings.");

                return View(model);
            }
          
        }


    }
}