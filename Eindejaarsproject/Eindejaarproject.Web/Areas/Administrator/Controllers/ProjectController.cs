﻿using Eindejaarproject.Web.Controllers;
using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.DA.Resources;
using Eindejaarsproject.DA.ViewModels;
using Eindejaarsproject.Services.IServices;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Eindejaarproject.Web.Areas.Administrator.Controllers
{

    [Authorize(Roles = "Administrator")]
    public class ProjectController : BaseController
    {
        #region Constructor
        private readonly IProjectRepository _projectRepository;
        private readonly IMailService _mailService;

        public ProjectController(IProjectRepository projectRepository, IMailService mailService)
        {
            this._projectRepository = projectRepository;
            this._mailService = mailService;
        }

        #endregion


        #region Actions

        [HttpGet]
        public async Task<ActionResult> List()
        {
            ProjectListViewModel model = new ProjectListViewModel()
            {
                ProjectForList = (await _projectRepository.GetProjectsForList(null, null)).ToList()
            };

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Details(int id)
        {
            ProjectDetailsViewModel model = await _projectRepository.GetProjectDetails(id);
            return View(model);

        }

        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {

            var isDeleted = await _projectRepository.DeleteProject(id);

            if (isDeleted)
            {
                AddDataToTempData("succes", Messages.ProjectDeleteSucces);
            }
            else
            {
                AddDataToTempData("danger", Messages.ProjectDeleteFailed);
            }



            return RedirectToAction("List");
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {

            UpsertProjectViewModel model =  await _projectRepository.GetUpsertProjectViewModel(id);
            model.Project.LanguagesForDropdown = await _projectRepository.GetApprovedLanguagesForDropDown();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(UpsertProjectViewModel model)
        {
         
            if (ModelState.IsValid)
            {
                var result = await _projectRepository.UpsertProject(model);
                AddDataToTempData("succes", Messages.ProjectEditSucces);
            }
            else
            {
                model.Project.LanguagesForDropdown = await _projectRepository.GetApprovedLanguagesForDropDown();
                AddDataToTempData("danger", Messages.ProjectEditFailed);
                return View(model);
            }
            return RedirectToAction("List");
        }

        #endregion




    }
}
