﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eindejaarproject.Web.Controllers;
using Eindejaarsproject.DA.IRepositories;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace Eindejaarproject.Web.Areas.Freelancer.Controllers
{
    [Authorize(Roles = "Administrator, Freelancer")]
    public class DeskController : BaseController
    {
        #region Constructor

        private readonly ISettingsRepository _settingsRepository;
        public DeskController(ISettingsRepository settingsRepository)
        {
            this._settingsRepository = settingsRepository;
        }
        #endregion


        public async Task<ActionResult> Dashboard()
        {
            var model = await _settingsRepository.GetDashboardViewModel(null, User.Identity.GetUserId());

            return View(model);
        }

    }
}