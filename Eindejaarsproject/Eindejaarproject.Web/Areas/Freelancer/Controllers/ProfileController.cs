﻿using Eindejaarproject.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eindejaarproject.Model.Models.Identity;
using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.DA.ViewModels;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace Eindejaarproject.Web.Areas.Freelancer.Controllers
{
    [Authorize(Roles = "Administrator, Freelancer")]
    public class ProfileController : BaseController
    {
        private readonly IUserRepository _userRepository;
        private readonly IFreelancerRepository _freelancerRepository;

        #region Constructor
        public ProfileController(IUserRepository userRepository, IFreelancerRepository freelancerRepository)
        {
            this._userRepository = userRepository;
            this._freelancerRepository = freelancerRepository;
        }
        #endregion

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var model = await _userRepository.GetUserDetails(User.Identity.GetUserId());
            model.LanguageCodes.Add("");
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Index(UserDetailsViewModel model)
        {
            if (model.LanguageCodes.Any() && model.LanguageCodes.Where(m => m == "").Count() == 0)
            {
          
                var freelancerLanguages = await _freelancerRepository.GetLanguagesByFreelancer(User.Identity.GetUserId());
                freelancerLanguages.AddRange(model.LanguageCodes);



                if (freelancerLanguages.GroupBy(m => m).Any(g => g.Count() > 1))
                {
                    var orgiginalModel = await _userRepository.GetUserDetails(User.Identity.GetUserId());
                    orgiginalModel.LanguageCodes.AddRange(model.LanguageCodes);
                    AddDataToTempData("danger", "Error");
                    ModelState.AddModelError("DuplicateLanguage", "Duplicate language found");
                    return View(orgiginalModel);
                }
                else
                {
                    _freelancerRepository.AddNewLanguages(model.LanguageCodes, User.Identity.GetUserId());
                    AddDataToTempData("succes", "Updated");
                    model = await _userRepository.GetUserDetails(User.Identity.GetUserId());
                    model.LanguageCodes.Add("");
                }
            }
            else
            {
                var orgiginalModel = await _userRepository.GetUserDetails(User.Identity.GetUserId());
                orgiginalModel.LanguageCodes.AddRange(model.LanguageCodes);
                AddDataToTempData("danger", "Error");
                ModelState.AddModelError("EmptyValueFound", "Empty value are not allowed.");
                return View(orgiginalModel);
            }
      

            return View(model);
        }

    }
}