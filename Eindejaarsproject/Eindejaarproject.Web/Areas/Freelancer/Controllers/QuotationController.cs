﻿using Eindejaarproject.Web.Controllers;
using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.DA.ViewModels;
using Eindejaarsproject.Services.IServices;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Eindejaarproject.Web.Areas.Freelancer.Controllers
{
    [Authorize(Roles = "Administrator, Freelancer")]
    public class QuotationController : BaseController
    {

        #region Constructor

        private readonly IQuotationRepository quotationRepository;
        private readonly IMailService _mailService;
        private readonly IUserRepository _userRepository;

        public QuotationController(IQuotationRepository quotationRepository, IMailService mailService, IUserRepository userRepository)
        {
            this.quotationRepository = quotationRepository;
            this._mailService = mailService;
            this._userRepository = userRepository;
        }

        #endregion


        [HttpGet]
        public async Task<ActionResult> List()
        {
            var freelancerId = User.Identity.GetUserId();
            var model = await quotationRepository.GetQuotationsForList(freelancerId);

            return View(model);
        }


        [HttpGet]
        [ValidateInput(false)]
        public async Task<ActionResult> Create(int projectId)
        {
            UpsertQuotationViewModel model = new UpsertQuotationViewModel();
            model.ProjectId = projectId;
            model.Quotations = await quotationRepository.GetQuotationsForList(null, projectId);

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(UpsertQuotationViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.AspNetUserId = User.Identity.GetUserId();
                model.ExpirationDate = Convert.ToDateTime(model.ExpirationDateString, new CultureInfo("nl-BE"));
                var userDetails = await  _userRepository.GetUserDetailsByProjectId(model.ProjectId);


                model.FirstName = userDetails.FirstName;
                model.LastName = userDetails.LastName;
                model.Address = userDetails.Address;


                var result = await quotationRepository.UpsertQuotation(model);

                //UserDetailsViewModel userDetails = await _userRepository.GetUserDetailsByProjectId(model.ProjectId);
                _mailService.SendMail(string.Format("{0} {1}", userDetails.FirstName, userDetails.LastName), new string[] { userDetails.Email }, "New Quotation", "There is a new quotation for one of your projects.");

                AddDataToTempData("succes", "Your quotation has been created");
                return RedirectToAction("List", "Project");

            }

            model.Quotations = await quotationRepository.GetQuotationsForList(null, model.ProjectId);
            AddDataToTempData("danger", "Error");
            return View(model);
        }



    }
}