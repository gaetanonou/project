﻿using Eindejaarproject.Web.Controllers;
using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.DA.ViewModels;
using Eindejaarsproject.Services.IServices;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Eindejaarproject.Web.Areas.Freelancer.Controllers
{
    [Authorize(Roles = "Administrator, Freelancer")]
    public class ProjectController : BaseController
    {
        #region Constructor
        private readonly IProjectRepository _projectRepository;
        private readonly IMailService _mailService;

        public ProjectController(IProjectRepository projectRepository, IMailService _mailService)
        {
            this._projectRepository = projectRepository;
            this._mailService = _mailService;
        }

        #endregion


        [HttpGet]
        public async Task<ActionResult> List()
        {

            var freelancerId = User.Identity.GetUserId();

            ProjectListViewModel model = new ProjectListViewModel()
            {
                ProjectForList = (await _projectRepository.GetProjectsForList(freelancerId, null)).ToList()
            };

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Details(int id)
        {
            ProjectDetailsViewModel model = await _projectRepository.GetProjectDetails(id);
            if (string.IsNullOrEmpty(model.FreelancerId) || model.FreelancerId == User.Identity.GetUserId())
            {
                return View(model);
            }
            else
            {
                return RedirectToAction("List");
            }


            return View(model);

        }

        [HttpGet]
        public async Task<ActionResult> Deliver(int id)
        {

            DeliverProjectViewModel model = new DeliverProjectViewModel();
            model.Id = id;
            return View(model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Deliver(DeliverProjectViewModel model)
        {
            var deadline = await _projectRepository.GetDeadlineForProject(model.Id);

            if (deadline < DateTime.UtcNow)
            {
                AddDataToTempData("danger", "This project has expired");
                return View(model);
            }

            if (ModelState.IsValid)
                {
                    await _projectRepository.Deliver(model);
                    

                    return RedirectToAction("List");
                }
           else
            {
                return View(model);
            }
        }
            

    }
}