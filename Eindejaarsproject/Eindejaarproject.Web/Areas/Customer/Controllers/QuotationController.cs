﻿using Eindejaarproject.Web.Controllers;
using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.Services.IServices;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Eindejaarproject.Web.Areas.Customer.Controllers
{
    [Authorize(Roles = "Administrator, Customer")]
    public class QuotationController : BaseController
    {
        #region Constructor
        private readonly IQuotationRepository quotationRepository;

        private readonly IUserRepository _userRepository;
        private readonly IPDFService _pdfService;
        private readonly IMailService _mailService;
        public QuotationController(IQuotationRepository quotationRepository, IPDFService pdfService, IMailService mailService, IUserRepository userRepository)
        {
            this.quotationRepository = quotationRepository;
            this._pdfService = pdfService;
            this._mailService = mailService;
            this._userRepository = userRepository;
        }

        #endregion

        [HttpGet]
        public async Task<ActionResult> List(int projectId)
        {
            var model = await quotationRepository.GetQuotationsForList(null,projectId);
            return View(model);
        }


        [HttpGet]
        public async Task<ActionResult> AcceptQuotation(int id)
        {
            var customerId = User.Identity.GetUserId();

            var result = await quotationRepository.AcceptQuotation(id, customerId);
            if(!string.IsNullOrEmpty(result))
            {
                var details = await _userRepository.GetUserDetails(result);


                if(details != null)
                {
                    _mailService.SendMail(string.Format("{0} {1}", details.FirstName, details.LastName), new string[] { details.Email }, "Quoatation accepted", "One of your quotation has been accepted.");
                }

                AddDataToTempData("succes", "Your quotation has been accepted.");
            }
            else
            {
                AddDataToTempData("danger", "An error occured while saving. Pleas try again.");
            }

            return RedirectToAction("List", "Project");
           
        }


        [HttpGet]
        public async Task<ActionResult> DownloadQuotation(int id, bool isInvoice = false)
        {
            var quotation = await quotationRepository.GetQuotationDetails(id);


            if(quotation == null)
            {

                return RedirectToAction("List", new { projectId = id });
                
            }

            MemoryStream file = _pdfService.CreateQuotation(quotation, isInvoice);



            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}-{1}.pdf", isInvoice ? "invoice" : "quatation",  id));
            Response.BinaryWrite(file.ToArray());
            return View();
        }
    }
}