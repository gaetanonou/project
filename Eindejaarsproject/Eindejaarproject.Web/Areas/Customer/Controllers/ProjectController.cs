﻿using Eindejaarproject.Web.Controllers;
using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.DA.ViewModels;
using Eindejaarsproject.DA.Resources;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Threading;
using System.Globalization;

namespace Eindejaarproject.Web.Areas.Customer.Controllers
{
    [Authorize(Roles = "Administrator, Customer")]
    public class ProjectController : BaseController
    {
        #region Constructor
        private readonly IProjectRepository projectRepository;

        public ProjectController(IProjectRepository projectRepository)
        {
            this.projectRepository = projectRepository;
        }

        #endregion


        #region Actions

        [HttpGet]
        public async Task<ActionResult> List()
        {

            var customerId = User.Identity.GetUserId();

            ProjectListViewModel model = new ProjectListViewModel()
            {
                ProjectForList = (await projectRepository.GetProjectsForList(null, customerId)).ToList()
            };

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Create()
        {
            UpsertProjectViewModel model = new UpsertProjectViewModel();
            model.Project.LanguagesForDropdown = await projectRepository.GetApprovedLanguagesForDropDown();

            //Set current date for deadline.
            model.Project.DesiredDeadlineString = DateTime.Now.ToString("dd/MM/yyyy");

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(UpsertProjectViewModel model)
        {
            model.Project.DesiredDeadline = Convert.ToDateTime(model.Project.DesiredDeadlineString, new CultureInfo("nl-BE"));
            model.Project.LanguagesForDropdown = await projectRepository.GetApprovedLanguagesForDropDown();

            if (ModelState.IsValid)
            {

                if(!model.Uploader.Files.Any())
                {
                    ModelState.AddModelError("Uploader.Files", Messages.UploadMinimumOneFile);
                    return View(model);
                }

                if (model.Project.DesiredDeadline <= DateTime.Now)
                {
                    ModelState.AddModelError("DesiredDeadlineIsLessOrEqualThanNow", Messages.DesiredDeadlineIsLessOrEqualThanNow);
                    return View(model);
                }


                model.Project.CustomerId = User.Identity.GetUserId();
                var result = await projectRepository.UpsertProject(model);
                AddDataToTempData("succes", Messages.ProjectCreateSucces);
                return RedirectToAction("Create");
            }
            else
            {
                AddDataToTempData("danger", Messages.ProjectCreateFailed);
                
                return View(model);
            }



        }


        [HttpGet]
        public async Task<ActionResult> Details(int id)
        {
            
            ProjectDetailsViewModel model = await projectRepository.GetProjectDetails(id);

            if(model.CustomerId == User.Identity.GetUserId())
            {
                return View(model);
            }
            else
            {
                return RedirectToAction("List");
            }
         

        }



        #endregion




    }
}