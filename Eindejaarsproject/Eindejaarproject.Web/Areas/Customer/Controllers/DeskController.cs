﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eindejaarsproject.DA.ViewModels;
using Eindejaarproject.Web.Controllers;
using Eindejaarsproject.Services.IServices;
using Eindejaarsproject.DA.IRepositories;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Eindejaarproject.Web.Areas.Customer.Controllers
{
    [Authorize(Roles = "Administrator, Customer")]
    public class DeskController : BaseController
    {
        #region Constructor

        private readonly ISettingsRepository _settingsRepository;
        public DeskController(ISettingsRepository settingsRepository)
        {
            this._settingsRepository = settingsRepository;
        }
        #endregion


        public async Task<ActionResult> Dashboard()
        {
            var model = await _settingsRepository.GetDashboardViewModel(User.Identity.GetUserId());

            return View(model);
        }

   



    }
}