﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.UI.WebControls;
using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.DA.Repositories;
using System.Net;

namespace Eindejaarproject.Web.Controllers
{
    [Authorize]
    public class ProjectController : ApiController
    {

        #region Ctor

        private readonly IProjectRepository _projectRepository;

        public ProjectController()
        {
            this._projectRepository = new ProjectRepository();
        }

    
        #endregion

        [HttpGet]
        public async Task<HttpResponseMessage> getMatchingLanguages(string languageCode)
        {
            var languages = await  _projectRepository.GetMatchingLanguagesForAPI(languageCode);



            return Request.CreateResponse(HttpStatusCode.OK, languages);
        }

    }
}