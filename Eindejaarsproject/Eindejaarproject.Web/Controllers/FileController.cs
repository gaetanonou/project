﻿using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.DA.Repositories;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Eindejaarproject.Web.Controllers
{
    [Authorize]
    public class FileController : ApiController
    {

        #region Contructor
        private readonly IBlobStorageRepository _blobStorageRepository;
        private readonly CloudStorageAccount _storageAccount;
        private readonly String container = "eindejaarsprojectcontainer";
        public FileController()
        {
            _storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString);
            this._blobStorageRepository = new BlobStorageRepository();
        }
        #endregion

        [HttpPost]
        public async Task<HttpResponseMessage> Upload()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            try
            {
                string root = HttpContext.Current.Server.MapPath("~/App_Data");
                if (!Directory.Exists(root))
                    Directory.CreateDirectory(root);

                var provider = new MultipartFormDataStreamProvider(root);

                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);

                List<String> blobFiles = new List<string>();



                // This illustrates how to get the file names. Only one file will be uploaded at a time.
                foreach (MultipartFileData file in provider.FileData)
                {

                    var contentType = file.Headers.ContentType.ToString().ToLower();
                    var originalFileName = file.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);

                    var ext = Path.GetExtension(originalFileName).Replace(".", "").ToLower();

                    if(ext != "jpg"
                        && ext !="jpeg"
                        && ext != "pjpeg"
                        && ext != "png"
                        && ext != "bmp"
                        && ext != "pdf"
                        && ext != "doc"
                        && ext != "docx"
                        && ext != "txt")
                    {
                        File.Delete(file.LocalFileName);
                        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                    }



                    var localFilePath = Path.GetFileName(file.LocalFileName);
                    blobFiles.Add(_blobStorageRepository.UploadBlobFromLocalFile(container, "Blob_" + Guid.NewGuid().ToString(), file.LocalFileName, contentType));


                    File.Delete(file.LocalFileName);
                }



                return Request.CreateResponse(HttpStatusCode.OK, blobFiles.First());
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> DownloadBlob(String blobName, String fileName)
        {
            CloudBlobClient blobClient = _storageAccount.CreateCloudBlobClient();
            CloudBlobContainer blobContainer = blobClient.GetContainerReference(container);
            CloudBlockBlob blob = blobContainer.GetBlockBlobReference(blobName);

            var blobExists = await blob.ExistsAsync();
            if (!blobExists)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "File not found");
            }

            HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.OK);
            Stream blobStream = await blob.OpenReadAsync();

            message.Content = new StreamContent(blobStream);
            message.Content.Headers.ContentLength = blob.Properties.Length;
            message.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(blob.Properties.ContentType);
            message.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = fileName,
                Size = blob.Properties.Length
            };

            return message;
        }


        [HttpPost]
        public async Task<HttpResponseMessage> DeleteBlob(String blobName)
        {
            if (!string.IsNullOrEmpty(blobName))
            {
                _blobStorageRepository.DeleteBlob(container, blobName);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }


    }

}
