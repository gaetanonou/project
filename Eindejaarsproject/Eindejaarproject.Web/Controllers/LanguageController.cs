﻿using System;
using System.Web.Mvc;
using System.Threading;
using System.Globalization;
using System.Web;

namespace Eindejaarproject.Web.Controllers
{
    public class LanguageController : BaseController
    {
        public ActionResult Change(String languageCode, String returnUrl)
        {
            if(!String.IsNullOrEmpty(languageCode))
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageCode);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageCode);

            }

            HttpCookie cookie = new HttpCookie("Language");
            cookie.Value = languageCode;
            Response.Cookies.Add(cookie);

            return Redirect(returnUrl);
        }
    }
}