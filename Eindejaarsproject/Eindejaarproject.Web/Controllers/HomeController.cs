﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eindejaarproject.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult News()
        {
          
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}