﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Eindejaarproject.Web.Controllers
{
    public class BaseController : Controller
    {

        public void AddDataToTempData(String key, String value)
        {
            if(TempData.ContainsKey(key))
            {
                TempData.Remove(key);
            }
            else
            {
                TempData.Add(key, value);
            };
        }
    }
}