﻿using Autofac;
using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.DA.Repositories;
using Eindejaarsproject.Model;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Eindejaarproject.Web.Startup))]
namespace Eindejaarproject.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        { 
            ConfigureAuth(app);
        }
    }
}
