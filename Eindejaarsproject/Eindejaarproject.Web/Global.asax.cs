﻿using Autofac;
using Autofac.Integration.Mvc;
using Eindejaarproject.Web.App_Start;
using Eindejaarsproject.DA.IRepositories;
using Eindejaarsproject.DA.Repositories;
using Eindejaarsproject.Model;
using Eindejaarsproject.Services;
using Eindejaarsproject.Services.IServices;
using Eindejaarsproject.Services.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Eindejaarproject.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
      

        protected void Application_Start()
        {

            var formatters = GlobalConfiguration.Configuration.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
          
          

            var builder = new ContainerBuilder();

            builder.RegisterControllers(Assembly.GetExecutingAssembly());


            // Register individual components
            builder.RegisterType<EindejaarsprojectContext>();
            builder.RegisterType<ProjectRepository>().As<IProjectRepository>();
            builder.RegisterType<BlobStorageRepository>().As<IBlobStorageRepository>();
            builder.RegisterType<QuotationRepository>().As<IQuotationRepository>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<FreelancerRepository>().As<IFreelancerRepository>();
            builder.RegisterType<SettingsRepository>().As<ISettingsRepository>();
            builder.RegisterType<MailService>().As<IMailService>();
            builder.RegisterType<PDFService>().As<IPDFService>();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));



        }


        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Language"];
            if(cookie != null && cookie.Value != null)
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cookie.Value);
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cookie.Value);
            }
            else
            {

                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-GB");
            }

        }
    }
}
