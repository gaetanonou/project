﻿using Eindejaarproject.Model.Models.Identity;
using Eindejaarsproject.Model.Models;
using Eindejaarsproject.Model.Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Eindejaarsproject.Model
{
    public class EindejaarsprojectContext :IdentityDbContext<ApplicationUser>  {




        public EindejaarsprojectContext()
            : base("EindejaarsprojectConnection", throwIfV1Schema: false)
        {
        }

        public DbSet<UserDetails> UserDetails { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Quotation> Quotations { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<TargetDocument> TargetDocuments { get; set; }
        public DbSet<SourceDocument> SourceDocuments { get; set; }
        public DbSet<Blob> Blobs { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<FreelancerLanguage> FreelancerLanguages { get; set; }
        public DbSet<Setting> Settings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TargetDocument>()
                .HasRequired(m => m.Blob)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SourceDocument>()
             .HasRequired(m => m.Blob)
             .WithMany()
             .WillCascadeOnDelete(false);

 

         

        
        }


        public static EindejaarsprojectContext Create()
        {

      

            return new EindejaarsprojectContext();
        }
        
    }
}
