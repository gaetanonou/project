namespace Eindejaarsproject.Model.Migrations
{
    using Eindejaarproject.Model.Models.Identity;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Eindejaarsproject.Model.EindejaarsprojectContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Eindejaarsproject.Model.EindejaarsprojectContext";

        }

        protected override void Seed(Eindejaarsproject.Model.EindejaarsprojectContext DbContext)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //



            if(!DbContext.Roles.Any(r => r.Name == "Administrator"))
            {
                DbContext.Roles.AddOrUpdate(new IdentityRole { Name = "Administrator" });
            }
            if (!DbContext.Roles.Any(r => r.Name == "Freelancer"))
            {
                DbContext.Roles.AddOrUpdate(new IdentityRole { Name = "Freelancer" });
            }
            if (!DbContext.Roles.Any(r => r.Name == "Customer"))
            {
                DbContext.Roles.AddOrUpdate(new IdentityRole { Name = "Customer" });
            }

            DbContext.SaveChanges();


            var userStore = new UserStore<ApplicationUser>(DbContext);
            var userManager = new UserManager<ApplicationUser>(userStore);

            string adminMail = ConfigurationManager.AppSettings["AdminEmailAddress"];

            if ((!DbContext.Users.Any(u => u.UserName == adminMail)))
            {
                userManager.Create(new ApplicationUser { UserName = adminMail, Email = adminMail, EmailConfirmed = true }, "Ivocursist#123");
                var user = userManager.FindByEmail(adminMail);
                userManager.AddToRole(user.Id, "Administrator");
            }
        }
    }
}
