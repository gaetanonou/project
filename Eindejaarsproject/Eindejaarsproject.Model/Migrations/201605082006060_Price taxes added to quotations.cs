namespace Eindejaarsproject.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Pricetaxesaddedtoquotations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Quotations", "PriceExclTaxInclCommission", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Quotations", "PriceInclTaxInclCommission", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Quotations", "PriceInclTaxInclCommission");
            DropColumn("dbo.Quotations", "PriceExclTaxInclCommission");
        }
    }
}
