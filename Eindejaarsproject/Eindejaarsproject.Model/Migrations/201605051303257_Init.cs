namespace Eindejaarsproject.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Street = c.String(),
                        ZipCode = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Blobs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FreelancerLanguages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AspNetUserId = c.String(maxLength: 128),
                        LanguageCode = c.String(),
                        Approved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.AspNetUserId)
                .Index(t => t.AspNetUserId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BlobId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Blobs", t => t.BlobId)
                .Index(t => t.BlobId);
            
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SourceLanguageCode = c.String(),
                        TargetLanguageCode = c.String(),
                        CustomerId = c.String(nullable: false, maxLength: 128),
                        FreelancerId = c.String(maxLength: 128),
                        Step = c.Int(nullable: false),
                        PaymentStep = c.Int(nullable: false),
                        AcceptedQuotationId = c.Int(),
                        InvoiceId = c.Int(),
                        IsSwornTranslation = c.Boolean(nullable: false),
                        NoteId = c.Int(),
                        CreationDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        IsDeleted = c.Boolean(nullable: false),
                        DesiredDeadline = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Deadline = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CustomerId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.FreelancerId)
                .ForeignKey("dbo.Invoices", t => t.InvoiceId)
                .ForeignKey("dbo.Notes", t => t.NoteId)
                .Index(t => t.CustomerId)
                .Index(t => t.FreelancerId)
                .Index(t => t.InvoiceId)
                .Index(t => t.NoteId);
            
            CreateTable(
                "dbo.Quotations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PriceExclTax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationDate = c.DateTime(nullable: false),
                        ExpirationDate = c.DateTime(nullable: false),
                        DeliveryPeriodExpressedInDays = c.Int(nullable: false),
                        ProjectId = c.Int(nullable: false),
                        AspNetUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.AspNetUserId)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .Index(t => t.ProjectId)
                .Index(t => t.AspNetUserId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaxPercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CommissionPercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SourceDocuments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        WordCount = c.Int(),
                        ProjectId = c.Int(nullable: false),
                        BlobId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Blobs", t => t.BlobId)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .Index(t => t.ProjectId)
                .Index(t => t.BlobId);
            
            CreateTable(
                "dbo.TargetDocuments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Extention = c.String(),
                        WordCount = c.Int(),
                        ProjectId = c.Int(nullable: false),
                        BlobId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Blobs", t => t.BlobId)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .Index(t => t.ProjectId)
                .Index(t => t.BlobId);
            
            CreateTable(
                "dbo.UserDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AspNetUserId = c.String(maxLength: 128),
                        FristName = c.String(),
                        LastName = c.String(),
                        BirthDay = c.DateTime(nullable: false),
                        AddressId = c.Int(nullable: false),
                        BillingAddressId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.AddressId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.AspNetUserId)
                .ForeignKey("dbo.Addresses", t => t.BillingAddressId)
                .Index(t => t.AspNetUserId)
                .Index(t => t.AddressId)
                .Index(t => t.BillingAddressId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserDetails", "BillingAddressId", "dbo.Addresses");
            DropForeignKey("dbo.UserDetails", "AspNetUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserDetails", "AddressId", "dbo.Addresses");
            DropForeignKey("dbo.TargetDocuments", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.TargetDocuments", "BlobId", "dbo.Blobs");
            DropForeignKey("dbo.SourceDocuments", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.SourceDocuments", "BlobId", "dbo.Blobs");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Quotations", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Quotations", "AspNetUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Projects", "NoteId", "dbo.Notes");
            DropForeignKey("dbo.Projects", "InvoiceId", "dbo.Invoices");
            DropForeignKey("dbo.Projects", "FreelancerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Projects", "CustomerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Invoices", "BlobId", "dbo.Blobs");
            DropForeignKey("dbo.FreelancerLanguages", "AspNetUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.UserDetails", new[] { "BillingAddressId" });
            DropIndex("dbo.UserDetails", new[] { "AddressId" });
            DropIndex("dbo.UserDetails", new[] { "AspNetUserId" });
            DropIndex("dbo.TargetDocuments", new[] { "BlobId" });
            DropIndex("dbo.TargetDocuments", new[] { "ProjectId" });
            DropIndex("dbo.SourceDocuments", new[] { "BlobId" });
            DropIndex("dbo.SourceDocuments", new[] { "ProjectId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Quotations", new[] { "AspNetUserId" });
            DropIndex("dbo.Quotations", new[] { "ProjectId" });
            DropIndex("dbo.Projects", new[] { "NoteId" });
            DropIndex("dbo.Projects", new[] { "InvoiceId" });
            DropIndex("dbo.Projects", new[] { "FreelancerId" });
            DropIndex("dbo.Projects", new[] { "CustomerId" });
            DropIndex("dbo.Invoices", new[] { "BlobId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.FreelancerLanguages", new[] { "AspNetUserId" });
            DropTable("dbo.UserDetails");
            DropTable("dbo.TargetDocuments");
            DropTable("dbo.SourceDocuments");
            DropTable("dbo.Settings");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Quotations");
            DropTable("dbo.Projects");
            DropTable("dbo.Notes");
            DropTable("dbo.Invoices");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.FreelancerLanguages");
            DropTable("dbo.Blobs");
            DropTable("dbo.Addresses");
        }
    }
}
