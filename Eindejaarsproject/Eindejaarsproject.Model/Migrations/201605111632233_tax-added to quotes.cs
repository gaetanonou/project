namespace Eindejaarsproject.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class taxaddedtoquotes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Quotations", "TaxPercentage", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Quotations", "TaxPercentage");
        }
    }
}
