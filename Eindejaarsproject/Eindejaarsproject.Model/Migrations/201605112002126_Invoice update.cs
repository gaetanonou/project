namespace Eindejaarsproject.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Invoiceupdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Invoices", "BlobId", "dbo.Blobs");
            DropIndex("dbo.Invoices", new[] { "BlobId" });
            AddColumn("dbo.Invoices", "CreationDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Invoices", "QuotationId", c => c.Int(nullable: false));
            CreateIndex("dbo.Invoices", "QuotationId");
            AddForeignKey("dbo.Invoices", "QuotationId", "dbo.Quotations", "Id", cascadeDelete: true);
            DropColumn("dbo.Invoices", "BlobId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Invoices", "BlobId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Invoices", "QuotationId", "dbo.Quotations");
            DropIndex("dbo.Invoices", new[] { "QuotationId" });
            DropColumn("dbo.Invoices", "QuotationId");
            DropColumn("dbo.Invoices", "CreationDate");
            CreateIndex("dbo.Invoices", "BlobId");
            AddForeignKey("dbo.Invoices", "BlobId", "dbo.Blobs", "Id");
        }
    }
}
