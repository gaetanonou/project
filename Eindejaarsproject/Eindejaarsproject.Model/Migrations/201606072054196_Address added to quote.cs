namespace Eindejaarsproject.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addressaddedtoquote : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Quotations", "FirstName", c => c.String());
            AddColumn("dbo.Quotations", "LastName", c => c.String());
            AddColumn("dbo.Quotations", "AddressId", c => c.Int(nullable: false));

            CreateIndex("dbo.Quotations", "AddressId");
            AddForeignKey("dbo.Quotations", "AddressId", "dbo.Addresses", "Id", cascadeDelete: true);

           
       
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Quotations", "AddressId", "dbo.Addresses");
            DropIndex("dbo.Quotations", new[] { "AddressId" });
            DropColumn("dbo.Quotations", "AddressId");
            DropColumn("dbo.Quotations", "LastName");
            DropColumn("dbo.Quotations", "FirstName");
        }
    }
}
