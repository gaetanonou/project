﻿using Eindejaarproject.Model.Models.Identity;
using Eindejaarsproject.Model.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.Model.Models
{
    public class Quotation
    { 
        [Key]
        public int Id { get; set; }

        public decimal PriceExclTax { get; set; }

        public decimal PriceExclTaxInclCommission { get; set; }

        public decimal PriceInclTaxInclCommission { get; set; }

        public decimal TaxPercentage { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime ExpirationDate { get; set; }

        public int DeliveryPeriodExpressedInDays { get; set; }

        [ForeignKey("Project")]
        public int ProjectId { get; set; }
        public Project Project { get; set; }

        [ForeignKey("ApplicationUser")]
        public String AspNetUserId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [ForeignKey("Address")]
        public int AddressId { get; set; }

        public Address Address { get; set; }


    }
}
