﻿
using Eindejaarproject.Model.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.Model.Models.Identity
{
    public class UserDetails
    {

        [Key]
        public int Id { get; set; }

        [ForeignKey("ApplicationUser")]
        public String AspNetUserId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        public String FristName { get; set; }

        public String LastName { get; set; }

        public DateTime BirthDay { get; set; }

        [ForeignKey("Address")]
        public int AddressId { get; set; }
        public Address Address { get; set; }

        [ForeignKey("BillingAddress")]
        public int? BillingAddressId { get; set; }

        public Address BillingAddress { get; set; }



    }
}
