﻿using Eindejaarproject.Model.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.Model.Models
{
    public class FreelancerLanguage
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("ApplicationUser")]
        public String AspNetUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public String LanguageCode { get; set; }

        public bool Approved { get; set; }
    }
}
