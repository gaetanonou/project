﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.Model.Models
{
    public static class Enums
    {
        public enum Roles
        {
            [Description("Administrator")]
            Administrator,
            [Description("SiteModerator")]
            SiteModerator,
            [Description("Customer")]
            Customer,
            [Description("Freelancer")]
            Freelancer
        }


        public enum ProjectStep
        {
            New,
            InProgress,
            Delivered,
            Complete
        }


        public enum PaymentStep
        {
            WaitingForQuotation,
            WaitingForPayment,
            Payed
        }




    }

    public static class RolesExtensions
    {
        public static string ToDescriptionString(this Eindejaarsproject.Model.Models.Enums.Roles val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
