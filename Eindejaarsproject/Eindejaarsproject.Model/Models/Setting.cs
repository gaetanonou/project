﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.Model.Models
{
    public class Setting
    {
        public int Id { get; set; }

        public decimal TaxPercentage { get; set; }

        public decimal CommissionPercentage { get; set; }

    }
}
