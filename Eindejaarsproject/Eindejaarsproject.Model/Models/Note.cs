﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.Model.Models
{
    public class Note
    {
        [Key]
        public int Id { get; set; }

        public String Content { get; set; }
    }
}
