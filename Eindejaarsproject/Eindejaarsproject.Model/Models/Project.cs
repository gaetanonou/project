﻿using Eindejaarproject.Model.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.Model.Models
{
    public class Project
    {
        [Key]
        public int Id { get; set; }


        public String SourceLanguageCode { get; set; }

        public String TargetLanguageCode { get; set; }

        public IEnumerable<SourceDocument> SourceDocuments { get; set; }

        public IEnumerable<TargetDocument> TargetDocuments { get; set; }

        [Required]
        [ForeignKey("Customer")]
        public String CustomerId { get; set; }

        public ApplicationUser Customer { get; set; }


        [ForeignKey("Freelancer")]
        public string FreelancerId { get; set; }

        public ApplicationUser Freelancer { get; set; }

        public Eindejaarsproject.Model.Models.Enums.ProjectStep Step { get; set; }

        public Eindejaarsproject.Model.Models.Enums.PaymentStep PaymentStep { get; set; }

        public IEnumerable<Quotation> Quotations { get; set; }
        public int? AcceptedQuotationId { get; set; }

        [ForeignKey("Invoice")]
        public int? InvoiceId { get; set; }
        public Invoice Invoice { get; set; }


        public bool IsSwornTranslation { get; set; }

        [ForeignKey("Note")]
        public int? NoteId { get; set; }
        public Note Note { get; set; }


        [Column(TypeName = "DateTime2")]
        public DateTime CreationDate { get; set; }

        public bool IsDeleted { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime DesiredDeadline { get; set; }

        public DateTime? Deadline { get; set; }


    }
}
