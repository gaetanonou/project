﻿using Eindejaarsproject.Model.Models.Identity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.Model.Models
{
    public class Address
    {
        [Key]
        public int Id { get; set; }

        public String Street { get; set; }

        public String ZipCode { get; set; }

        public String City { get; set; }

        public String State { get; set; }

        public String Country { get; set; }
    }
}
