﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.Model.Models
{
    public class TargetDocument
    {
        [Key]
        public int Id { get; set; }

        public String Name { get; set; }

        public String Extention { get; set; }

        public int? WordCount { get; set; }

        [ForeignKey("Project")]
        public int ProjectId { get; set; }

        public Project Project { get; set; }

        [ForeignKey("Blob")]
        public int BlobId { get; set; }

        public Blob Blob { get; set; }
    }
}
