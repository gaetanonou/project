﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eindejaarsproject.Model.Models
{
    public class Invoice
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreationDate { get; set; }

        [ForeignKey("Quotation")]
        public int QuotationId { get; set; }

        public Quotation Quotation { get; set; }
    }
}
